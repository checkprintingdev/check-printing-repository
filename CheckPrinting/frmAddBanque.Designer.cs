﻿namespace CheckPrinting
{
    partial class frmAddBanque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddBanque));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtSearch = new MetroFramework.Controls.MetroTextBox();
            this.LibelleBanque = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodeBanque = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LibelleCourt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Picture = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gvChequiers = new MetroFramework.Controls.MetroGrid();
            this.btnModifier = new MetroFramework.Controls.MetroButton();
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.banqueListView = new System.Windows.Forms.ListView();
            this.metroTabChequier = new MetroFramework.Controls.MetroTabControl();
            this.metroPageChequier = new MetroFramework.Controls.MetroTabPage();
            this.txtLibelleBanque = new MetroFramework.Controls.MetroTextBox();
            this.txtLibelleCourt = new MetroFramework.Controls.MetroTextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.txtCodeBanque = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lblFilename = new MetroFramework.Controls.MetroLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.imageListBanques = new System.Windows.Forms.ImageList(this.components);
            this.imageCombo1 = new System.Windows.Forms.ImageCombo();
            ((System.ComponentModel.ISupportInitialize)(this.gvChequiers)).BeginInit();
            this.metroTabChequier.SuspendLayout();
            this.metroPageChequier.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtSearch
            // 
            // 
            // 
            // 
            this.txtSearch.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            this.txtSearch.CustomButton.Location = new System.Drawing.Point(321, 1);
            this.txtSearch.CustomButton.Name = "";
            this.txtSearch.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSearch.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSearch.CustomButton.TabIndex = 1;
            this.txtSearch.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSearch.CustomButton.UseSelectable = true;
            this.txtSearch.DisplayIcon = true;
            this.txtSearch.Icon = ((System.Drawing.Image)(resources.GetObject("txtSearch.Icon")));
            this.txtSearch.Lines = new string[0];
            this.txtSearch.Location = new System.Drawing.Point(674, 120);
            this.txtSearch.MaxLength = 32767;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.PasswordChar = '\0';
            this.txtSearch.PromptText = "Search";
            this.txtSearch.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSearch.SelectedText = "";
            this.txtSearch.SelectionLength = 0;
            this.txtSearch.SelectionStart = 0;
            this.txtSearch.ShortcutsEnabled = true;
            this.txtSearch.ShowButton = true;
            this.txtSearch.Size = new System.Drawing.Size(343, 23);
            this.txtSearch.TabIndex = 88;
            this.txtSearch.UseSelectable = true;
            this.txtSearch.WaterMark = "Search";
            this.txtSearch.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSearch.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // LibelleBanque
            // 
            this.LibelleBanque.Text = "LIBELLE BANQUE";
            this.LibelleBanque.Width = 151;
            // 
            // CodeBanque
            // 
            this.CodeBanque.Text = "CODE BANQUE";
            this.CodeBanque.Width = 173;
            // 
            // LibelleCourt
            // 
            this.LibelleCourt.Text = "LIBELLE COURT";
            this.LibelleCourt.Width = 298;
            // 
            // Picture
            // 
            this.Picture.Text = "PICTURE";
            this.Picture.Width = 794;
            // 
            // gvChequiers
            // 
            this.gvChequiers.AllowUserToAddRows = false;
            this.gvChequiers.AllowUserToDeleteRows = false;
            this.gvChequiers.AllowUserToResizeRows = false;
            this.gvChequiers.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gvChequiers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gvChequiers.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gvChequiers.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvChequiers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gvChequiers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvChequiers.DefaultCellStyle = dataGridViewCellStyle2;
            this.gvChequiers.EnableHeadersVisualStyles = false;
            this.gvChequiers.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gvChequiers.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gvChequiers.Location = new System.Drawing.Point(1, 16);
            this.gvChequiers.Name = "gvChequiers";
            this.gvChequiers.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvChequiers.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gvChequiers.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gvChequiers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvChequiers.Size = new System.Drawing.Size(985, 643);
            this.gvChequiers.TabIndex = 2;
            this.gvChequiers.Visible = false;
            // 
            // btnModifier
            // 
            this.btnModifier.Location = new System.Drawing.Point(1401, 561);
            this.btnModifier.Margin = new System.Windows.Forms.Padding(2);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(117, 36);
            this.btnModifier.TabIndex = 68;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseSelectable = true;
            this.btnModifier.Visible = false;
            // 
            // metroLink1
            // 
            this.metroLink1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLink1.Image = ((System.Drawing.Image)(resources.GetObject("metroLink1.Image")));
            this.metroLink1.ImageSize = 32;
            this.metroLink1.Location = new System.Drawing.Point(1840, 15);
            this.metroLink1.Margin = new System.Windows.Forms.Padding(2);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.NoFocusImage = ((System.Drawing.Image)(resources.GetObject("metroLink1.NoFocusImage")));
            this.metroLink1.Size = new System.Drawing.Size(24, 26);
            this.metroLink1.TabIndex = 70;
            this.metroLink1.TabStop = false;
            this.metroLink1.UseSelectable = true;
            // 
            // banqueListView
            // 
            this.banqueListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.CodeBanque,
            this.LibelleBanque,
            this.LibelleCourt,
            this.Picture});
            this.banqueListView.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.banqueListView.FullRowSelect = true;
            this.banqueListView.GridLines = true;
            this.banqueListView.HideSelection = false;
            this.banqueListView.Location = new System.Drawing.Point(3, 16);
            this.banqueListView.Name = "banqueListView";
            this.banqueListView.Size = new System.Drawing.Size(981, 476);
            this.banqueListView.TabIndex = 3;
            this.banqueListView.UseCompatibleStateImageBehavior = false;
            this.banqueListView.View = System.Windows.Forms.View.Details;
            this.banqueListView.SelectedIndexChanged += new System.EventHandler(this.banqueListView_SelectedIndexChanged);
            // 
            // metroTabChequier
            // 
            this.metroTabChequier.Controls.Add(this.metroPageChequier);
            this.metroTabChequier.Location = new System.Drawing.Point(22, 145);
            this.metroTabChequier.Name = "metroTabChequier";
            this.metroTabChequier.SelectedIndex = 0;
            this.metroTabChequier.Size = new System.Drawing.Size(995, 566);
            this.metroTabChequier.TabIndex = 72;
            this.metroTabChequier.UseSelectable = true;
            // 
            // metroPageChequier
            // 
            this.metroPageChequier.Controls.Add(this.banqueListView);
            this.metroPageChequier.Controls.Add(this.gvChequiers);
            this.metroPageChequier.HorizontalScrollbarBarColor = true;
            this.metroPageChequier.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPageChequier.HorizontalScrollbarSize = 10;
            this.metroPageChequier.Location = new System.Drawing.Point(4, 38);
            this.metroPageChequier.Name = "metroPageChequier";
            this.metroPageChequier.Size = new System.Drawing.Size(987, 524);
            this.metroPageChequier.TabIndex = 0;
            this.metroPageChequier.Text = "Banque";
            this.metroPageChequier.VerticalScrollbarBarColor = true;
            this.metroPageChequier.VerticalScrollbarHighlightOnWheel = false;
            this.metroPageChequier.VerticalScrollbarSize = 10;
            // 
            // txtLibelleBanque
            // 
            // 
            // 
            // 
            this.txtLibelleBanque.CustomButton.Image = null;
            this.txtLibelleBanque.CustomButton.Location = new System.Drawing.Point(216, 2);
            this.txtLibelleBanque.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleBanque.CustomButton.Name = "";
            this.txtLibelleBanque.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtLibelleBanque.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLibelleBanque.CustomButton.TabIndex = 1;
            this.txtLibelleBanque.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLibelleBanque.CustomButton.UseSelectable = true;
            this.txtLibelleBanque.CustomButton.Visible = false;
            this.txtLibelleBanque.Lines = new string[0];
            this.txtLibelleBanque.Location = new System.Drawing.Point(1208, 173);
            this.txtLibelleBanque.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleBanque.MaxLength = 32767;
            this.txtLibelleBanque.Name = "txtLibelleBanque";
            this.txtLibelleBanque.PasswordChar = '\0';
            this.txtLibelleBanque.PromptText = "Libellé Banque";
            this.txtLibelleBanque.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLibelleBanque.SelectedText = "";
            this.txtLibelleBanque.SelectionLength = 0;
            this.txtLibelleBanque.SelectionStart = 0;
            this.txtLibelleBanque.ShortcutsEnabled = true;
            this.txtLibelleBanque.Size = new System.Drawing.Size(236, 22);
            this.txtLibelleBanque.TabIndex = 63;
            this.txtLibelleBanque.UseSelectable = true;
            this.txtLibelleBanque.WaterMark = "Libellé Banque";
            this.txtLibelleBanque.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLibelleBanque.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtLibelleCourt
            // 
            // 
            // 
            // 
            this.txtLibelleCourt.CustomButton.Image = null;
            this.txtLibelleCourt.CustomButton.Location = new System.Drawing.Point(216, 2);
            this.txtLibelleCourt.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleCourt.CustomButton.Name = "";
            this.txtLibelleCourt.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtLibelleCourt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLibelleCourt.CustomButton.TabIndex = 1;
            this.txtLibelleCourt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLibelleCourt.CustomButton.UseSelectable = true;
            this.txtLibelleCourt.CustomButton.Visible = false;
            this.txtLibelleCourt.Lines = new string[0];
            this.txtLibelleCourt.Location = new System.Drawing.Point(1208, 229);
            this.txtLibelleCourt.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleCourt.MaxLength = 32767;
            this.txtLibelleCourt.Name = "txtLibelleCourt";
            this.txtLibelleCourt.PasswordChar = '\0';
            this.txtLibelleCourt.PromptText = "Libellé Court";
            this.txtLibelleCourt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLibelleCourt.SelectedText = "";
            this.txtLibelleCourt.SelectionLength = 0;
            this.txtLibelleCourt.SelectionStart = 0;
            this.txtLibelleCourt.ShortcutsEnabled = true;
            this.txtLibelleCourt.Size = new System.Drawing.Size(236, 22);
            this.txtLibelleCourt.TabIndex = 62;
            this.txtLibelleCourt.UseSelectable = true;
            this.txtLibelleCourt.WaterMark = "Libellé Court";
            this.txtLibelleCourt.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLibelleCourt.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.statusStrip1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(20, 846);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(1836, 24);
            this.statusStrip1.TabIndex = 71;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.ActiveLinkColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.LinkColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(139, 19);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.VisitedLinkColor = System.Drawing.Color.White;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(1401, 521);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(117, 36);
            this.btnSave.TabIndex = 69;
            this.btnSave.Text = "Save";
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(1046, 296);
            this.metroLabel11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(46, 19);
            this.metroLabel11.TabIndex = 49;
            this.metroLabel11.Text = "Image";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(1054, 173);
            this.metroLabel8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(94, 19);
            this.metroLabel8.TabIndex = 51;
            this.metroLabel8.Text = "Libellé Banque";
            // 
            // txtCodeBanque
            // 
            // 
            // 
            // 
            this.txtCodeBanque.CustomButton.Image = null;
            this.txtCodeBanque.CustomButton.Location = new System.Drawing.Point(216, 2);
            this.txtCodeBanque.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeBanque.CustomButton.Name = "";
            this.txtCodeBanque.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtCodeBanque.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCodeBanque.CustomButton.TabIndex = 1;
            this.txtCodeBanque.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCodeBanque.CustomButton.UseSelectable = true;
            this.txtCodeBanque.CustomButton.Visible = false;
            this.txtCodeBanque.Lines = new string[0];
            this.txtCodeBanque.Location = new System.Drawing.Point(1208, 120);
            this.txtCodeBanque.MaxLength = 32767;
            this.txtCodeBanque.Name = "txtCodeBanque";
            this.txtCodeBanque.PasswordChar = '\0';
            this.txtCodeBanque.PromptText = "Code Banque";
            this.txtCodeBanque.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCodeBanque.SelectedText = "";
            this.txtCodeBanque.SelectionLength = 0;
            this.txtCodeBanque.SelectionStart = 0;
            this.txtCodeBanque.ShortcutsEnabled = true;
            this.txtCodeBanque.Size = new System.Drawing.Size(236, 22);
            this.txtCodeBanque.TabIndex = 59;
            this.txtCodeBanque.UseSelectable = true;
            this.txtCodeBanque.WaterMark = "Code Banque";
            this.txtCodeBanque.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCodeBanque.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(1054, 229);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(84, 19);
            this.metroLabel6.TabIndex = 55;
            this.metroLabel6.Text = "Libellé Court";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(1054, 120);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(89, 19);
            this.metroLabel1.TabIndex = 47;
            this.metroLabel1.Text = "Code Banque";
            // 
            // lblFilename
            // 
            this.lblFilename.AutoSize = true;
            this.lblFilename.Location = new System.Drawing.Point(1194, 366);
            this.lblFilename.Name = "lblFilename";
            this.lblFilename.Size = new System.Drawing.Size(0, 0);
            this.lblFilename.TabIndex = 91;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1205, 278);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 95;
            this.label1.Text = "Image Path:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(1208, 295);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(241, 20);
            this.textBox1.TabIndex = 94;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(1208, 331);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(310, 174);
            this.pictureBox1.TabIndex = 93;
            this.pictureBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(1455, 292);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(63, 23);
            this.button2.TabIndex = 92;
            this.button2.Text = "Upload";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // imageListBanques
            // 
            this.imageListBanques.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageListBanques.ImageSize = new System.Drawing.Size(16, 16);
            this.imageListBanques.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // imageCombo1
            // 
            this.imageCombo1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.imageCombo1.FormattingEnabled = true;
            this.imageCombo1.Location = new System.Drawing.Point(1208, 576);
            this.imageCombo1.Name = "imageCombo1";
            this.imageCombo1.Size = new System.Drawing.Size(168, 21);
            this.imageCombo1.TabIndex = 96;
            this.imageCombo1.Visible = false;
            // 
            // frmAddBanque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1876, 890);
            this.Controls.Add(this.imageCombo1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.lblFilename);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.btnModifier);
            this.Controls.Add(this.metroLink1);
            this.Controls.Add(this.metroTabChequier);
            this.Controls.Add(this.txtLibelleBanque);
            this.Controls.Add(this.txtLibelleCourt);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.txtCodeBanque);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel1);
            this.Name = "frmAddBanque";
            this.Text = "Banque Infos";
            this.Load += new System.EventHandler(this.frmAddBanque_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.gvChequiers)).EndInit();
            this.metroTabChequier.ResumeLayout(false);
            this.metroPageChequier.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroTextBox txtSearch;
        private System.Windows.Forms.ColumnHeader LibelleBanque;
        private System.Windows.Forms.ColumnHeader CodeBanque;
        private System.Windows.Forms.ColumnHeader LibelleCourt;
        private System.Windows.Forms.ColumnHeader Picture;
        private MetroFramework.Controls.MetroGrid gvChequiers;
        private MetroFramework.Controls.MetroButton btnModifier;
        private MetroFramework.Controls.MetroLink metroLink1;
        private System.Windows.Forms.ListView banqueListView;
        private MetroFramework.Controls.MetroTabControl metroTabChequier;
        private MetroFramework.Controls.MetroTabPage metroPageChequier;
        private MetroFramework.Controls.MetroTextBox txtLibelleBanque;
        private MetroFramework.Controls.MetroTextBox txtLibelleCourt;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private MetroFramework.Controls.MetroButton btnSave;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox txtCodeBanque;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblFilename;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ImageCombo imageCombo1;
        private System.Windows.Forms.ImageList imageListBanques;
    }
}