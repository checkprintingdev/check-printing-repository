﻿namespace CheckPrinting
{
    partial class frmStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStart));
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.modernButton1 = new CustomControls.Controls.ModernButton();
            this.modernButton2 = new CustomControls.Controls.ModernButton();
            this.modernButton3 = new CustomControls.Controls.ModernButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.metroLink2 = new MetroFramework.Controls.MetroLink();
            this.modernButton4 = new CustomControls.Controls.ModernButton();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLink1
            // 
            this.metroLink1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLink1.Image = ((System.Drawing.Image)(resources.GetObject("metroLink1.Image")));
            this.metroLink1.ImageSize = 32;
            this.metroLink1.Location = new System.Drawing.Point(727, 6);
            this.metroLink1.Margin = new System.Windows.Forms.Padding(2);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.NoFocusImage = ((System.Drawing.Image)(resources.GetObject("metroLink1.NoFocusImage")));
            this.metroLink1.Size = new System.Drawing.Size(33, 33);
            this.metroLink1.TabIndex = 7;
            this.metroLink1.TabStop = false;
            this.metroLink1.UseSelectable = true;
            this.metroLink1.Click += new System.EventHandler(this.metroLink1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(2, 6);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(238, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // modernButton1
            // 
            this.modernButton1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.modernButton1.AnimationSizeDecrement = 15F;
            this.modernButton1.AnimationSizeIncrement = 30F;
            this.modernButton1.BackColor = System.Drawing.Color.Transparent;
            this.modernButton1.BorderRadius = 10;
            this.modernButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.modernButton1.FontSize = CustomControls.Controls.CustomFontSize.Tall;
            this.modernButton1.FontStyle = System.Drawing.FontStyle.Regular;
            this.modernButton1.Icon = ((System.Drawing.Image)(resources.GetObject("modernButton1.Icon")));
            this.modernButton1.LineThickness = 3;
            this.modernButton1.Location = new System.Drawing.Point(136, 178);
            this.modernButton1.Name = "modernButton1";
            this.modernButton1.Size = new System.Drawing.Size(242, 81);
            this.modernButton1.TabIndex = 8;
            this.modernButton1.TabStop = false;
            this.modernButton1.Text = "Gestion Stock";
            this.modernButton1.UseIcon = true;
            this.modernButton1.Click += new System.EventHandler(this.modernButton1_Click);
            // 
            // modernButton2
            // 
            this.modernButton2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.modernButton2.AnimationSizeDecrement = 15F;
            this.modernButton2.AnimationSizeIncrement = 30F;
            this.modernButton2.BackColor = System.Drawing.Color.Transparent;
            this.modernButton2.BorderRadius = 10;
            this.modernButton2.ColorStyle = CustomControls.MyColorStyle.AutumnMapple;
            this.modernButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.modernButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modernButton2.FontSize = CustomControls.Controls.CustomFontSize.Tall;
            this.modernButton2.FontStyle = System.Drawing.FontStyle.Regular;
            this.modernButton2.Icon = ((System.Drawing.Image)(resources.GetObject("modernButton2.Icon")));
            this.modernButton2.LineThickness = 3;
            this.modernButton2.Location = new System.Drawing.Point(181, 265);
            this.modernButton2.Name = "modernButton2";
            this.modernButton2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.modernButton2.Size = new System.Drawing.Size(221, 81);
            this.modernButton2.TabIndex = 9;
            this.modernButton2.TabStop = false;
            this.modernButton2.Text = "Impression";
            this.modernButton2.UseIcon = true;
            this.modernButton2.Click += new System.EventHandler(this.modernButton2_Click);
            // 
            // modernButton3
            // 
            this.modernButton3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.modernButton3.AnimationSizeDecrement = 15F;
            this.modernButton3.AnimationSizeIncrement = 30F;
            this.modernButton3.BackColor = System.Drawing.Color.Transparent;
            this.modernButton3.BorderRadius = 10;
            this.modernButton3.ColorStyle = CustomControls.MyColorStyle.TawnyPort;
            this.modernButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.modernButton3.FontSize = CustomControls.Controls.CustomFontSize.Tall;
            this.modernButton3.FontStyle = System.Drawing.FontStyle.Regular;
            this.modernButton3.Icon = ((System.Drawing.Image)(resources.GetObject("modernButton3.Icon")));
            this.modernButton3.LineThickness = 3;
            this.modernButton3.Location = new System.Drawing.Point(393, 178);
            this.modernButton3.Name = "modernButton3";
            this.modernButton3.Size = new System.Drawing.Size(236, 81);
            this.modernButton3.TabIndex = 10;
            this.modernButton3.TabStop = false;
            this.modernButton3.Text = "Paramétrage";
            this.modernButton3.UseIcon = true;
            this.modernButton3.Click += new System.EventHandler(this.modernButton3_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.panel1.Location = new System.Drawing.Point(65, 157);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(473, 5);
            this.panel1.TabIndex = 12;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.panel2.Location = new System.Drawing.Point(224, 357);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(473, 5);
            this.panel2.TabIndex = 13;
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.statusStrip1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 480);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(762, 24);
            this.statusStrip1.TabIndex = 15;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.ActiveLinkColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.LinkColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(139, 19);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.VisitedLinkColor = System.Drawing.Color.White;
            // 
            // metroLink2
            // 
            this.metroLink2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLink2.Image = ((System.Drawing.Image)(resources.GetObject("metroLink2.Image")));
            this.metroLink2.ImageSize = 32;
            this.metroLink2.Location = new System.Drawing.Point(690, 6);
            this.metroLink2.Margin = new System.Windows.Forms.Padding(2);
            this.metroLink2.Name = "metroLink2";
            this.metroLink2.NoFocusImage = ((System.Drawing.Image)(resources.GetObject("metroLink2.NoFocusImage")));
            this.metroLink2.Size = new System.Drawing.Size(33, 33);
            this.metroLink2.TabIndex = 16;
            this.metroLink2.TabStop = false;
            this.metroLink2.UseSelectable = true;
            this.metroLink2.Click += new System.EventHandler(this.metroLink2_Click);
            // 
            // modernButton4
            // 
            this.modernButton4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.modernButton4.AnimationSizeDecrement = 15F;
            this.modernButton4.AnimationSizeIncrement = 30F;
            this.modernButton4.BackColor = System.Drawing.Color.Transparent;
            this.modernButton4.BorderRadius = 10;
            this.modernButton4.ColorStyle = CustomControls.MyColorStyle.Greenery;
            this.modernButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.modernButton4.FontSize = CustomControls.Controls.CustomFontSize.Tall;
            this.modernButton4.FontStyle = System.Drawing.FontStyle.Regular;
            this.modernButton4.Icon = ((System.Drawing.Image)(resources.GetObject("modernButton4.Icon")));
            this.modernButton4.LineThickness = 3;
            this.modernButton4.Location = new System.Drawing.Point(408, 270);
            this.modernButton4.Name = "modernButton4";
            this.modernButton4.Size = new System.Drawing.Size(242, 81);
            this.modernButton4.TabIndex = 8;
            this.modernButton4.TabStop = false;
            this.modernButton4.Text = "Gestion Chéquiers";
            this.modernButton4.UseIcon = true;
            this.modernButton4.Click += new System.EventHandler(this.modernButton1_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.label1.Location = new System.Drawing.Point(74, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 37);
            this.label1.TabIndex = 18;
            this.label1.Text = "Check Printing";
            // 
            // frmStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 504);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.metroLink2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.modernButton3);
            this.Controls.Add(this.modernButton2);
            this.Controls.Add(this.modernButton4);
            this.Controls.Add(this.modernButton1);
            this.Controls.Add(this.metroLink1);
            this.Controls.Add(this.pictureBox1);
            this.DisplayHeader = false;
            this.MaximizeBox = false;
            this.Movable = false;
            this.Name = "frmStart";
            this.Padding = new System.Windows.Forms.Padding(0, 30, 0, 0);
            this.Resizable = false;
            this.Load += new System.EventHandler(this.frmStart_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLink metroLink1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private CustomControls.Controls.ModernButton modernButton1;
        private CustomControls.Controls.ModernButton modernButton2;
        private CustomControls.Controls.ModernButton modernButton3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private MetroFramework.Controls.MetroLink metroLink2;
        private CustomControls.Controls.ModernButton modernButton4;
        private System.Windows.Forms.Label label1;
    }
}