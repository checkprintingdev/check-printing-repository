﻿using MetroFramework.Forms;
using System;
using System.Windows.Forms;
using MetroFramework;

namespace CheckPrinting
{
    public partial class frmParametrage : MetroForm
    {
        public frmParametrage()
        {
            InitializeComponent();
            this.Size = Screen.FromRectangle(this.Bounds).WorkingArea.Size;
        }
        private void frmGestionChequiers_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = DateTime.Now.ToLongDateString();
        }

        private void metroLink2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void metroTile4_Click(object sender, EventArgs e)
        {
            frmStart f = new frmStart();
            f.Show();
            Hide();
        }

        private void metroLink1_Click(object sender, EventArgs e)
        {
            Close();
            frmStart f = new frmStart();
            f.Show();
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            frmAddEditChequier f = new frmAddEditChequier();
            f.Show();
            Hide();
        }

        private void metroTile3_Click(object sender, EventArgs e)
        {

        }

        private void metroTile2_Click(object sender, EventArgs e)
        {

        }

        private void metroLink1_Click_1(object sender, EventArgs e)
        {
            Close();
            frmStart f = new frmStart();
            f.Show();
        }

        private void metroTile2_Click_1(object sender, EventArgs e)
        {
            frmAddBanque f = new frmAddBanque();
            f.Show();
            Hide();
        }
    }
}
