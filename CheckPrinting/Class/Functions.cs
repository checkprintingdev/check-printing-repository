﻿using CheckPrinting.Properties;
using CheckPrinting_DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CheckPrinting.Class
{
    static class Functions
    {
        public static string ConnectionString
        {
            get
            {
                return "Server=" + Settings.Default.Server + ";Port=" + Settings.Default.Port + ";Database=" + Settings.Default.Database + ";Uid=" + Settings.Default.Username + ";Pwd=" + Settings.Default.Password + ";";
            }
        }

        public static bool TestConnection()
        {
            bool _return = true;

            //using (SqlConnection con = new SqlConnection(ConnectionString))
            //{
            //    try
            //    {
            //        con.Open();
            //    }
            //    catch (Exception)
            //    {
            //        _return = false;
            //    }
            //}
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork ;
            try
            {
                unitOfWork = new UnitOfWork(parameterConnection);
               int nbrBanque =unitOfWork.context.Banques.Count();
            }
            catch (Exception)
            {
                _return = false;
            }
            return _return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns>DataTable</returns>
        public static DataTable DataTable(string sql)
        {
            DataTable _table = new DataTable();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using(SqlDataAdapter adp = new SqlDataAdapter(sql, con))
                {
                    adp.Fill(_table);
                }
            }

            return _table;
        }
    }
}
