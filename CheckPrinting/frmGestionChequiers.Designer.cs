﻿namespace CheckPrinting
{
    partial class frmGestionChequiers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGestionChequiers));
            this.yearlyToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.singleTripToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.cabinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlyToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.yearlyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sIngleTripToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vehicleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yearlyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singleTripToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manifestoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.metroContextMenu1 = new MetroFramework.Controls.MetroContextMenu(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.metroTile4 = new MetroFramework.Controls.MetroTile();
            this.metroTile2 = new MetroFramework.Controls.MetroTile();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroTile3 = new MetroFramework.Controls.MetroTile();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.metroContextMenu1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // yearlyToolStripMenuItem2
            // 
            this.yearlyToolStripMenuItem2.Name = "yearlyToolStripMenuItem2";
            this.yearlyToolStripMenuItem2.Size = new System.Drawing.Size(148, 24);
            this.yearlyToolStripMenuItem2.Text = "Yearly";
            // 
            // singleTripToolStripMenuItem2
            // 
            this.singleTripToolStripMenuItem2.Name = "singleTripToolStripMenuItem2";
            this.singleTripToolStripMenuItem2.Size = new System.Drawing.Size(148, 24);
            this.singleTripToolStripMenuItem2.Text = "Single Trip";
            // 
            // cabinToolStripMenuItem
            // 
            this.cabinToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.singleTripToolStripMenuItem2,
            this.monthlyToolStripMenuItem2,
            this.yearlyToolStripMenuItem2});
            this.cabinToolStripMenuItem.Name = "cabinToolStripMenuItem";
            this.cabinToolStripMenuItem.Size = new System.Drawing.Size(144, 24);
            this.cabinToolStripMenuItem.Text = "Cabin";
            // 
            // monthlyToolStripMenuItem2
            // 
            this.monthlyToolStripMenuItem2.Name = "monthlyToolStripMenuItem2";
            this.monthlyToolStripMenuItem2.Size = new System.Drawing.Size(148, 24);
            this.monthlyToolStripMenuItem2.Text = "Monthly";
            // 
            // yearlyToolStripMenuItem1
            // 
            this.yearlyToolStripMenuItem1.Name = "yearlyToolStripMenuItem1";
            this.yearlyToolStripMenuItem1.Size = new System.Drawing.Size(148, 24);
            this.yearlyToolStripMenuItem1.Text = "Yearly";
            // 
            // monthlyToolStripMenuItem1
            // 
            this.monthlyToolStripMenuItem1.Name = "monthlyToolStripMenuItem1";
            this.monthlyToolStripMenuItem1.Size = new System.Drawing.Size(148, 24);
            this.monthlyToolStripMenuItem1.Text = "Monthly";
            // 
            // sIngleTripToolStripMenuItem1
            // 
            this.sIngleTripToolStripMenuItem1.Name = "sIngleTripToolStripMenuItem1";
            this.sIngleTripToolStripMenuItem1.Size = new System.Drawing.Size(148, 24);
            this.sIngleTripToolStripMenuItem1.Text = "Single Trip";
            // 
            // vehicleToolStripMenuItem
            // 
            this.vehicleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sIngleTripToolStripMenuItem1,
            this.monthlyToolStripMenuItem1,
            this.yearlyToolStripMenuItem1});
            this.vehicleToolStripMenuItem.Name = "vehicleToolStripMenuItem";
            this.vehicleToolStripMenuItem.Size = new System.Drawing.Size(144, 24);
            this.vehicleToolStripMenuItem.Text = "Vehicle";
            // 
            // yearlyToolStripMenuItem
            // 
            this.yearlyToolStripMenuItem.Name = "yearlyToolStripMenuItem";
            this.yearlyToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.yearlyToolStripMenuItem.Text = "Yearly";
            // 
            // monthlyToolStripMenuItem
            // 
            this.monthlyToolStripMenuItem.Name = "monthlyToolStripMenuItem";
            this.monthlyToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.monthlyToolStripMenuItem.Text = "Monthly";
            // 
            // singleTripToolStripMenuItem
            // 
            this.singleTripToolStripMenuItem.Name = "singleTripToolStripMenuItem";
            this.singleTripToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.singleTripToolStripMenuItem.Text = "Single Trip";
            // 
            // seatToolStripMenuItem
            // 
            this.seatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.singleTripToolStripMenuItem,
            this.monthlyToolStripMenuItem,
            this.yearlyToolStripMenuItem});
            this.seatToolStripMenuItem.Name = "seatToolStripMenuItem";
            this.seatToolStripMenuItem.Size = new System.Drawing.Size(144, 24);
            this.seatToolStripMenuItem.Text = "Seat";
            // 
            // manifestoToolStripMenuItem
            // 
            this.manifestoToolStripMenuItem.Name = "manifestoToolStripMenuItem";
            this.manifestoToolStripMenuItem.Size = new System.Drawing.Size(144, 24);
            this.manifestoToolStripMenuItem.Text = "Manifesto";
            // 
            // metroContextMenu1
            // 
            this.metroContextMenu1.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.metroContextMenu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manifestoToolStripMenuItem,
            this.seatToolStripMenuItem,
            this.vehicleToolStripMenuItem,
            this.cabinToolStripMenuItem});
            this.metroContextMenu1.Name = "metroContextMenu1";
            this.metroContextMenu1.Size = new System.Drawing.Size(145, 100);
            this.metroContextMenu1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroContextMenu1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroContextMenu1.UseStyleColors = true;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel2.Location = new System.Drawing.Point(42, 187);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(620, 4);
            this.panel2.TabIndex = 15;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Location = new System.Drawing.Point(139, 372);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(620, 4);
            this.panel1.TabIndex = 14;
            // 
            // metroLink1
            // 
            this.metroLink1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLink1.Image = ((System.Drawing.Image)(resources.GetObject("metroLink1.Image")));
            this.metroLink1.ImageSize = 32;
            this.metroLink1.Location = new System.Drawing.Point(751, 8);
            this.metroLink1.Margin = new System.Windows.Forms.Padding(2);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.NoFocusImage = ((System.Drawing.Image)(resources.GetObject("metroLink1.NoFocusImage")));
            this.metroLink1.Size = new System.Drawing.Size(24, 26);
            this.metroLink1.TabIndex = 13;
            this.metroLink1.TabStop = false;
            this.metroLink1.UseSelectable = true;
            this.metroLink1.Click += new System.EventHandler(this.metroLink1_Click);
            // 
            // metroTile4
            // 
            this.metroTile4.ActiveControl = null;
            this.metroTile4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroTile4.Location = new System.Drawing.Point(557, 200);
            this.metroTile4.Margin = new System.Windows.Forms.Padding(2);
            this.metroTile4.Name = "metroTile4";
            this.metroTile4.Size = new System.Drawing.Size(150, 162);
            this.metroTile4.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroTile4.TabIndex = 12;
            this.metroTile4.TabStop = false;
            this.metroTile4.Text = "Accueil";
            this.metroTile4.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTile4.TileImage")));
            this.metroTile4.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile4.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTile4.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.metroTile4.UseSelectable = true;
            this.metroTile4.UseTileImage = true;
            this.metroTile4.Click += new System.EventHandler(this.metroTile4_Click);
            // 
            // metroTile2
            // 
            this.metroTile2.ActiveControl = null;
            this.metroTile2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroTile2.Location = new System.Drawing.Point(248, 200);
            this.metroTile2.Margin = new System.Windows.Forms.Padding(2);
            this.metroTile2.Name = "metroTile2";
            this.metroTile2.Size = new System.Drawing.Size(150, 162);
            this.metroTile2.Style = MetroFramework.MetroColorStyle.Magenta;
            this.metroTile2.TabIndex = 10;
            this.metroTile2.TabStop = false;
            this.metroTile2.Text = "Modification Chéquier";
            this.metroTile2.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTile2.TileImage")));
            this.metroTile2.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile2.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTile2.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.metroTile2.UseSelectable = true;
            this.metroTile2.UseTileImage = true;
            this.metroTile2.Click += new System.EventHandler(this.metroTile2_Click);
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            this.metroTile1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroTile1.Location = new System.Drawing.Point(70, 200);
            this.metroTile1.Margin = new System.Windows.Forms.Padding(2);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(173, 162);
            this.metroTile1.TabIndex = 9;
            this.metroTile1.TabStop = false;
            this.metroTile1.Text = "Nouveau Chéquier";
            this.metroTile1.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTile1.TileImage")));
            this.metroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTile1.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.metroTile1.UseSelectable = true;
            this.metroTile1.UseTileImage = true;
            this.metroTile1.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(5, 8);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(238, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // metroTile3
            // 
            this.metroTile3.ActiveControl = null;
            this.metroTile3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.metroTile3.Location = new System.Drawing.Point(403, 200);
            this.metroTile3.Margin = new System.Windows.Forms.Padding(2);
            this.metroTile3.Name = "metroTile3";
            this.metroTile3.Size = new System.Drawing.Size(150, 162);
            this.metroTile3.Style = MetroFramework.MetroColorStyle.Green;
            this.metroTile3.TabIndex = 11;
            this.metroTile3.TabStop = false;
            this.metroTile3.Text = "Rapports";
            this.metroTile3.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTile3.TileImage")));
            this.metroTile3.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile3.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTile3.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular;
            this.metroTile3.UseSelectable = true;
            this.metroTile3.UseTileImage = true;
            this.metroTile3.Click += new System.EventHandler(this.metroTile3_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.statusStrip1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(15, 386);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(749, 24);
            this.statusStrip1.TabIndex = 16;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.ActiveLinkColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.LinkColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(139, 19);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.VisitedLinkColor = System.Drawing.Color.White;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.label1.Location = new System.Drawing.Point(35, 148);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(265, 37);
            this.label1.TabIndex = 17;
            this.label1.Text = "Gestion Chéquier";
            // 
            // frmGestionChequiers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 426);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.metroLink1);
            this.Controls.Add(this.metroTile4);
            this.Controls.Add(this.metroTile2);
            this.Controls.Add(this.metroTile1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.metroTile3);
            this.DisplayHeader = false;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Movable = false;
            this.Name = "frmGestionChequiers";
            this.Padding = new System.Windows.Forms.Padding(15, 30, 15, 16);
            this.Resizable = false;
            this.Text = "frmGestionChequiers";
            this.metroContextMenu1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem yearlyToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem singleTripToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem cabinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlyToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem yearlyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem monthlyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sIngleTripToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vehicleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yearlyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singleTripToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manifestoToolStripMenuItem;
        private MetroFramework.Controls.MetroContextMenu metroContextMenu1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private MetroFramework.Controls.MetroLink metroLink1;
        private MetroFramework.Controls.MetroTile metroTile4;
        private MetroFramework.Controls.MetroTile metroTile2;
        private MetroFramework.Controls.MetroTile metroTile1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroTile metroTile3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Label label1;
    }
}