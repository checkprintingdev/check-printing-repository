﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using CheckPrinting.Class;
using CheckPrinting_DAL;
using MetroFramework;

namespace CheckPrinting
{
    public partial class frmAddEditChequier : MetroFramework.Forms.MetroForm
    {
        public frmAddEditChequier()
        {
            InitializeComponent();
            this.Size = Screen.FromRectangle(this.Bounds).WorkingArea.Size;
        }
        Image ConvertBinaryToImage(byte[] data)
        {
            using (MemoryStream ms = new MemoryStream(data))
            {
                return Image.FromStream(ms);
            }
        }
        private void loadBanques()
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            List<Banque> listeBanques = new List<Banque>();
            listeBanques = unitOfWork.banqueRepository.Get().ToList();
            this.imageListBanques.TransparentColor = Color.Transparent;
           // cmbBanque2.Items.Add(new ImageComboItem("Select an Item", -1));
            int i = 0;
            //Fill Rows
            foreach (Banque banque in listeBanques)
            {
                this.imageListBanques.Images.Add(i.ToString(), ConvertBinaryToImage(banque.Picture));
                cmbBanque2.Items.Add(new ImageComboItem(banque.LibelleCourt, i ,true,Color.Black ,(object) banque.CodeBanque));
                i = i + 1;

            }
            cmbBanque2.ImageList = imageListBanques;
            cmbBanque2.SelectedIndex = 0;
        }

        private void frmAddEditChequier_Load(object sender, EventArgs e)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            List<Banque> listeBanque = unitOfWork.banqueRepository.Get().ToList();
            cmbBanque.DataSource = listeBanque;
            //cmbBanque.DataBindings;
            cmbBanque.DisplayMember = "LibelleCourt";
            loadBanques();
            List<Devise> listeDevise = unitOfWork.deviseRepository.Get().ToList();
            cmbDevise.DataSource = listeDevise;
            cmbDevise.DisplayMember = "LibelleDevise";
            List<TypeProduit> listeTypeProduit = unitOfWork.typeProduitRepository.Get().ToList();
            cmbTypeProduit.DataSource = listeTypeProduit;
            cmbTypeProduit.DisplayMember = "Libelle";
            loadChequiers("");
            toolStripStatusLabel1.Text = DateTime.Now.ToLongDateString();
            Clear();
        }
        private void loadChequiers(string filter)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            // DataTable dt = makeDataTable();
            List<Chequier> listeChequiers = new List<Chequier>();
            if(filter=="")
            {
                listeChequiers = unitOfWork.chequierRepository.Get().ToList();
            }
            else
            {
                listeChequiers = unitOfWork.chequierRepository.Get().ToList().Where(x=>x.RIB.Contains(filter.Trim())|| x.RefChquier.Contains(filter.Trim())|| x.TitulaireCompte.ToUpper().Contains(filter.ToUpper().Trim())).ToList();
            }
            gvChequiers.DataSource = listeChequiers;


            chequierListView.BeginUpdate();
            chequierListView.Items.Clear();
            chequierListView.View = View.Details;
            chequierListView.CheckBoxes = true;

            //Fill Rows
            foreach (Chequier chequier in listeChequiers)
            {
                ListViewItem lvi;
                lvi = new ListViewItem(new string[] { chequier.RefChquier , chequier.TitulaireCompte ,chequier.RIB ,chequier.NumPreCh.ToString(), chequier.NumDerCh.ToString(),
                chequier.NbreCh.ToString() ,chequier.DateCommandeBanque.ToString() ,chequier.LieuPayement.ToString(),chequier.AdrPayement.ToString(),chequier.TelAgence.ToString(),
                 chequier.Etat.ToString() ,chequier.JourneeBanque.ToString() ,chequier.DateTraitement.ToString(),chequier.Residence.ToString(),chequier.CodeDevise.ToString(),
                 chequier.DateLivraison.ToString() ,chequier.DateBLClient.ToString() ,chequier.CodeCentreImpr.ToString(),chequier.CodeBanque.ToString(),chequier.CodeAgence.ToString(),
                 chequier.CodeProduit.ToString() 
                });
                chequierListView.Items.Add(lvi);
                chequierListView.Items[0].Checked = true;
                
            }

            chequierListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            chequierListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            chequierListView.EndUpdate();
            //chequierListView.AllowSorting = true;
        }
        private void metroLink2_Click(object sender, EventArgs e)
        {
            frmGestionChequiers f = new frmGestionChequiers();
            f.Show();
            Hide();
        }

        private void metroLink1_Click(object sender, EventArgs e)
        {
            Close();
            frmGestionChequiers f = new frmGestionChequiers();
            f.Show();
        }

        private DataTable makeDataTable()
        {
            DataTable workTable = new DataTable("Chequier");
            DataColumn workCol = workTable.Columns.Add("RefChquier", typeof(string));
            workCol.AllowDBNull = false;
            workCol.Unique = true;
            workTable.Columns.Add("RIB", typeof(string));
            workTable.Columns.Add("NumPreCh", typeof(string));
            workTable.Columns.Add("NumDerCh", typeof(string));
            workTable.Columns.Add("NbreCh", typeof(string));
             workTable.Columns.Add("DateCommandeBanque", typeof(DateTime));
             workTable.Columns.Add("TitulaireCompte", typeof(string));
             workTable.Columns.Add("CodeBanque", typeof(string));

            DataRow workRow = workTable.NewRow();
            workRow["RefChquier"] = "1";
            workRow["RIB"] = "123456";
            workRow["NumPreCh"] = "1";
            workRow["NumDerCh"] = "50";
            workRow["NbreCh"] = "50";
            workRow["DateCommandeBanque"] = new DateTime(2020 ,10,30);
            workRow["TitulaireCompte"] = "HRIZI Mohamed";
            workRow["CodeBanque"] = "01";

            workTable.Rows.Add(workRow);

            workRow = workTable.NewRow();
            workRow["RefChquier"] = "2";
            workRow["RIB"] = "123456";
            workRow["NumPreCh"] = "1";
            workRow["NumDerCh"] = "50";
            workRow["NbreCh"] = "50";
            workRow["DateCommandeBanque"] = new DateTime(2020, 10, 30);
            workRow["TitulaireCompte"] = "HRIZI Mohamed";
            workRow["CodeBanque"] = "01";

            workTable.Rows.Add(workRow);

            workRow = workTable.NewRow();
            workRow["RefChquier"] = "3";
            workRow["RIB"] = "123456";
            workRow["NumPreCh"] = "1";
            workRow["NumDerCh"] = "50";
            workRow["NbreCh"] = "50";
            workRow["DateCommandeBanque"] = new DateTime(2020, 10, 30);
            workRow["TitulaireCompte"] = "HRIZI Mohamed";
            workRow["CodeBanque"] = "01";

            workTable.Rows.Add(workRow);

            return workTable;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            Chequier chequier = new Chequier();
            Banque selectedBanque = cmbBanque.SelectedValue as Banque;
            Produit selectedProduit = cmbProduit.SelectedValue as Produit;
            Agence selectedAgence = cmbAgence.SelectedValue as Agence;
            Devise selectedDevise = cmbDevise.SelectedValue as Devise;
            string codeBanque = selectedBanque.CodeBanque.Trim();
            string quantieme = DateTime.Now.DayOfYear.ToString();
            string journeeQuantieme = "1";
            //Refjournée = Code banque(char[3]) + Quantième(N° jour de l’année)(char[3]) + Journée dans quantième(char[1])
            string refJournee = codeBanque + quantieme + journeeQuantieme;
            //Réfchéquier = Refjournée + RIB(char[20]) + NumDeb(char[8]) + NumFin(char[8])
            string refChequier = refJournee + txtRIB.Text.ToString().Trim() + Fill_With_Char(txtNumPremChequier.Text.ToString().Trim(), '0', 8, 0) + Fill_With_Char(txtNumDerChequier.Text.ToString().Trim(), '0', 8, 0);
            chequier.RefChquier = refChequier;
            chequier.RIB = txtRIB.Text.Trim();
            chequier.NumPreCh =Convert.ToInt32( txtNumPremChequier.Text.Trim());
	        chequier.NumDerCh = Convert.ToInt32(txtNumDerChequier.Text.Trim());
            chequier.NbreCh = Convert.ToInt32(txtNbreCheque.Text.Trim());
            chequier.DateCommandeBanque = DateTime.Now;
            chequier.TitulaireCompte = txtClient.Text.ToString().Trim();
	        chequier.LieuPayement=txtLieuPayement.Text.ToString().Trim();
            chequier.AdrPayement = txtAdresse.Text.ToString().Trim();
            chequier.TelAgence = txtTelAgence.Text.ToString().Trim();
            chequier.Etat = chkEtat.Checked?"A":"N";
            chequier.JourneeBanque = null;
            chequier.DateTraitement = null;
            chequier.Residence = txtResidence.Text.ToString().Trim();
            chequier.CodeDevise = selectedDevise.CodeDevise.Trim();
            chequier.DateLivraison = null;
            chequier.DateBLClient = null;
            chequier.CodeCentreImpr = "";
            chequier.CodeBanque = selectedBanque.CodeBanque.Trim();
            chequier.CodeAgence = selectedAgence.CodeAgence.Trim();
            chequier.CodeProduit = selectedProduit.CodeProduit.Trim();

           string result= unitOfWork.chequierRepository.Insert(chequier).ToString();
            if(result== "Unchanged")
            {
                MetroMessageBox.Show(this, "Chéquier ajouté avec succès", "Success",
                          MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MetroMessageBox.Show(this, "Une erreur est survenue.", "Error",
                                 MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

           
            loadChequiers("");

            Clear();

        }


        public string Fill_With_Char(string StartString, char MyChar, int length, int Orientation)
        {
            string ret = "";
            string deb = "";

            for (int i = 1; i <= length - StartString.Length; i++)
            {
                deb = deb + MyChar;
            }
            if (Orientation == 0)
            {
                ret = deb + StartString;
            }
            else
            {
                ret = StartString + deb;
            }

            return ret;
        }

        private void cmbBanque_SelectedIndexChanged(object sender, EventArgs e)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            Banque selectedBanque = cmbBanque.SelectedValue as Banque;
            List<Agence> listeAgence = selectedBanque.Agences.ToList();    //unitOfWork.agenceRepository.Get().ToList().Where(x=>x.CodeBanque== selectedBanque.CodeBanque.Trim()).ToList();
            cmbAgence.DataSource = listeAgence;
            //cmbBanque.DataBindings;
            cmbAgence.DisplayMember = "LibelleAgence";
        }

        private void cmbTypeProduit_SelectedIndexChanged(object sender, EventArgs e)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            Banque selectedBanque = cmbBanque.SelectedValue as Banque;
            TypeProduit selectedTypeProduit = cmbTypeProduit.SelectedValue as TypeProduit;
            List<Produit> listeProduit = unitOfWork.produitRepository.Get().Where(x => x.CodeBanque == selectedBanque.CodeBanque.Trim() && x.TypeProd == selectedTypeProduit.CodeTypeProduit.Trim()).ToList();
            cmbProduit.DataSource = listeProduit;
            //cmbBanque.DataBindings;
            cmbProduit.DisplayMember = "Libelle";
        }

        public bool ControleRib(string p_Rib)
        {
            string l_Rib;
            double l_Rib1;
            double l_Reste1;
            string l_Rib2;
            double l_Reste2;

            string l_Crest1;
            double l_Rib3;
            long l_ZCle;
            string l_CZCle;
            string l_CleRib;
            bool _return = false;
            //Currency l_Rib4;

            l_Rib = p_Rib;
            if ((l_Rib.Length != 20))
            {
                _return = false;
            }
            else
            {
                l_Rib1 = double.Parse(l_Rib.Substring(0, 10));
                l_Reste1 = l_Rib1 % 97;
                if (l_Reste1 < 10)
                {
                    l_Crest1 = "0" + l_Reste1.ToString().Trim().Substring(0, 1);
                }
                else
                {
                    l_Crest1 = l_Reste1.ToString().Trim().Substring(0, 2);
                }

                l_Rib2 = (l_Crest1 + (l_Rib.Substring(10, 8) + "00"));
                l_Rib3 = double.Parse(l_Rib2);
                l_Reste2 = l_Rib3 % 97;
                l_ZCle = Convert.ToInt64(97 - l_Reste2);
                if ((l_ZCle < 10))
                {
                    l_CZCle = ("0" + l_ZCle.ToString().Trim().Substring(0, 1));
                }
                else
                {
                    l_CZCle = l_ZCle.ToString().Trim().Substring(0, 2);
                }
                l_CleRib = l_Rib.Substring((l_Rib.Length - 2));
                if ((l_CZCle != l_CleRib))
                {
                    _return = false;
                }
                else
                {
                    _return = true;
                }

            }
            return _return;
        }

        

        /// <summary>
        /// Vérifie la validité d'un RIB
        /// </summary>
        /// <param name="rib">Le RIB à vérifier</param>
        /// <returns>true si le RIB est valide, false sinon</returns>
        public static bool IsValidRib(string rib)
        {
           Regex regex_rib=null;
           // Suppression des espaces et tirets
           string tmp = rib.Replace(" ", "").Replace("-", "");

            // Vérification du format BBBBBGGGGGCCCCCCCCCCCKK
            // B : banque (5)
            // G : guichet(5)
            // C : numéro de compte(11)
            // K : clé RIB(2)
            // 14 809 8091007070951 56   (BH)
            // Code Banque Code Agence Numéro de compte Clé  (  97 - ((89 * b + 15 * g + 76 * d + 3 * c) % 97)
            if (regex_rib == null)
            {
                regex_rib = new Regex(@"(?<B>\d{2})(?<G>\d{3})(?<C>\w{13})(?<K>\d{2})", RegexOptions.Compiled);
            }
            Match m = regex_rib.Match(tmp);
            if (!m.Success)
                return false;

            // Extraction des composants
            string b_s = m.Groups["B"].Value;
            string g_s = m.Groups["G"].Value;
            string c_s = m.Groups["C"].Value;
            string k_s = m.Groups["K"].Value;

            // Remplacement des lettres par des chiffres dans le numéro de compte
            StringBuilder sb = new StringBuilder();
            foreach (char ch in c_s.ToUpper())
            {
                if (char.IsDigit(ch))
                    sb.Append(ch);
                else
                    sb.Append(RibLetterToDigit(ch));
            }
            c_s = sb.ToString();

            // Séparation du numéro de compte pour tenir sur 32bits
            string d_s = c_s.Substring(0, 6);
            c_s = c_s.Substring(6, 5);

            // Calcul de la clé RIB
            // Algo ici : http://fr.wikipedia.org/wiki/Clé_RIB#Algorithme_de_calcul_qui_fonctionne_avec_des_entiers_32_bits

            int b = int.Parse(b_s);
            int g = int.Parse(g_s);
            int d = int.Parse(d_s);
            int c = int.Parse(c_s);
            int k = int.Parse(k_s);

            int calculatedKey = 97 - ((89 * b + 15 * g + 76 * d + 3 * c) % 97);

            return (k == calculatedKey);
        }

        /// <summary>
        /// Convertit une lettre d'un RIB en un chiffre selon la table suivante :
        /// 1 2 3 4 5 6 7 8 9
        /// A B C D E F G H I
        /// J K L M N O P Q R
        /// _ S T U V W X Y Z
        /// </summary>
        /// <param name="letter">La lettre à convertir</param>
        /// <returns>Le chiffre de remplacement</returns>
        public static char RibLetterToDigit(char letter)
        {
            if (letter >= 'A' && letter <= 'I')
            {
                return (char)(letter - 'A' + '1');
            }
            else if (letter >= 'J' && letter <= 'R')
            {
                return (char)(letter - 'J' + '1');
            }
            else if (letter >= 'S' && letter <= 'Z')
            {
                return (char)(letter - 'S' + '2');
            }
            else
                throw new ArgumentOutOfRangeException("Le caractère à convertir doit être une lettre majuscule dans la plage A-Z");
        }

        private void txtRIB_TextChanged(object sender, EventArgs e)
        {
            ControleRib(txtRIB.Text);
        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            loadChequiers(txtSearch.Text.ToString().Trim());
        }

        
        private void chequierListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            if (chequierListView.SelectedIndices.Count <= 0)
            {
                return;
            }
            ListViewItem item = chequierListView.FocusedItem as ListViewItem;
            string refChequier= item.Text.ToString();
            Chequier chequier = unitOfWork.chequierRepository.Get().ToList().Where(x => x.RefChquier == refChequier).FirstOrDefault();
            if(chequier !=null)
            {
                txtRefChequier.Text= chequier.RefChquier ;
                txtRIB.Text=chequier.RIB ;
                txtNumPremChequier.Text= chequier.NumPreCh.ToString() ;
                txtNumDerChequier.Text=chequier.NumDerCh.ToString();
                txtNbreCheque.Text=chequier.NbreCh.ToString();
                //chequier.DateCommandeBanque = DateTime.Now;
                txtClient.Text=chequier.TitulaireCompte;
                txtLieuPayement.Text=chequier.LieuPayement;
                txtAdresse.Text=chequier.AdrPayement;
                txtTelAgence.Text=chequier.TelAgence;
                chkEtat.Checked =chequier.Etat =="A"? true : false;
                //chequier.JourneeBanque = null;
                //chequier.DateTraitement = null;
                txtResidence.Text=chequier.Residence ;
                Devise devise = new Devise();
                for (int i = 0; i < cmbDevise.Items.Count; i++)
                {
                    devise = cmbDevise.Items[i] as Devise;
                    if (devise.CodeDevise == chequier.CodeDevise)
                    {
                        cmbDevise.SelectedItem = (object)cmbDevise.Items[i];
                        break;
                    }
                }
                Banque banque = new Banque();
                for (int i = 0; i < cmbBanque.Items.Count; i++)
                {
                    banque = cmbBanque.Items[i] as Banque;
                    if (banque.CodeBanque == chequier.CodeBanque)
                    {
                        cmbBanque.SelectedItem = (object)cmbBanque.Items[i];
                        break;
                    }
                }
                //chequier.DateLivraison = null;
                //chequier.DateBLClient = null;
                //chequier.CodeCentreImpr = "";
                //chequier.CodeAgence = selectedAgence.CodeAgence.Trim();
                //cmbProduit.SelectedValue=chequier.CodeProduit;
            }
            btnModifier.Visible = true;
            btnSave.Visible = false;

        }

        private void btnModifier_Click(object sender, EventArgs e)
        {

        }

        private void cmbBanque2_SelectedIndexChanged(object sender, EventArgs e)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
           // Banque selectedBanque = cmbBanque2.SelectedValue as Banque;
            ImageComboItem banqueItem = cmbBanque2.SelectedItem as ImageComboItem;
            string codeBanque = banqueItem.Tag.ToString().Trim();
            Banque selectedBanque = unitOfWork.banqueRepository.Get().ToList().Where(x => x.CodeBanque == codeBanque).FirstOrDefault();
            // cmbAgence.Items.Clear();
            List <Agence> listeAgence = new List<Agence>();
            if(selectedBanque != null)
            {
                listeAgence= selectedBanque.Agences.ToList();    //unitOfWork.agenceRepository.Get().ToList().Where(x=>x.CodeBanque== selectedBanque.CodeBanque.Trim()).ToList();
                cmbAgence.DataSource = listeAgence;
                //cmbBanque.DataBindings;
                cmbAgence.DisplayMember = "LibelleAgence";
            }
            referenceChequier();

        }

        private void txtNbreCheque_Leave(object sender, EventArgs e)
        {
            referenceChequier();
        }

        private void referenceChequier()
        {
            ImageComboItem banqueItem = cmbBanque2.SelectedItem as ImageComboItem;
            string codeBanque = banqueItem.Tag.ToString().Trim();
            string quantieme = DateTime.Now.DayOfYear.ToString();
            string journeeQuantieme = "1";
            //Refjournée = Code banque(char[3]) + Quantième(N° jour de l’année)(char[3]) + Journée dans quantième(char[1])
            string refJournee = codeBanque + quantieme + journeeQuantieme;
            //Réfchéquier = Refjournée + RIB(char[20]) + NumDeb(char[8]) + NumFin(char[8])
            string refChequier = refJournee + txtRIB.Text.ToString().Trim() + Fill_With_Char(txtNumPremChequier.Text.ToString().Trim(), '0', 8, 0) + Fill_With_Char(txtNumDerChequier.Text.ToString().Trim(), '0', 8, 0);
            txtRefChequier.Text = refChequier;
        }

        private void txtNumPremChequier_Leave(object sender, EventArgs e)
        {
            referenceChequier();
        }

        private void txtNumDerChequier_Leave(object sender, EventArgs e)
        {
            referenceChequier();
        }

        private void txtRIB_Leave(object sender, EventArgs e)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            Chequier lastChequier = new Chequier();
            long? numPremierChequier = 0;
            lastChequier = unitOfWork.chequierRepository.Get().Where(x => x.RIB == txtRIB.Text.ToString().Trim()).OrderByDescending(x => x.NumDerCh).FirstOrDefault();
            if(lastChequier!=null)
            {
                numPremierChequier = lastChequier.NumDerCh + 1;
            }
            txtNumPremChequier.Text = numPremierChequier.ToString();
            referenceChequier();
        }

        private void Clear()
        {
        txtRIB.Text="";
        txtNumPremChequier.Text="0";
        txtNumDerChequier.Text="0";
        txtNbreCheque.Text="0";
        metroDateTime1.Text = DateTime.Now.ToString();
        txtClient.Text="";
        txtLieuPayement.Text = "";
        txtAdresse.Text = "";
        txtTelAgence.Text = "";
        chkEtat.Checked =false;
        txtResidence.Text = "";
        cmbBanque.SelectedIndex=0;
        cmbDevise.SelectedIndex = 0;
        cmbAgence.SelectedIndex = 0;
        cmbProduit.SelectedIndex = 0;
        cmbTypeProduit.SelectedIndex = 0;
        }

        private void btnGenereLigneCheq_Click(object sender, EventArgs e)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            List<Chequier> listeChequiers = new List<Chequier>();
            List<DetailsChequier> listeDetailsChequier = new List<DetailsChequier>();
            List<DetailsChequier> detailsChequier = new List<DetailsChequier>();
            listeChequiers = unitOfWork.chequierRepository.Get().ToList();
            foreach(Chequier chequier in listeChequiers)
            {
                detailsChequier = genererDetailsChequier(chequier);
                listeDetailsChequier.AddRange(detailsChequier);
                detailsChequier = new List<DetailsChequier>();
            }
            unitOfWork.detailsChequierRepository.InsertMany(listeDetailsChequier);
            
        }
        private List<DetailsChequier> genererDetailsChequier(Chequier chequier)
        {
            List<DetailsChequier> listeDetailsChequier = new List<DetailsChequier>();
            DetailsChequier detailsChequier = new DetailsChequier();
            long? numPremCheq = chequier.NumPreCh;
            long? numDernCheq = chequier.NumDerCh;
            for (long? i= numPremCheq;i<= numDernCheq; i++)
            {
                detailsChequier.CodeProduit = chequier.CodeProduit;
                detailsChequier.CodeTypeDoc = "";
                detailsChequier.NumVignette = "";
                detailsChequier.page = "";
                detailsChequier.position = "";
                detailsChequier.RefChquier = chequier.RefChquier;
                detailsChequier.RefVignette = "";
                detailsChequier.RIB = chequier.RIB;
                detailsChequier.TitulaireCompte = chequier.TitulaireCompte;
                listeDetailsChequier.Add(detailsChequier);
                detailsChequier = new DetailsChequier();
            }

            return listeDetailsChequier;
        }
    }
}
