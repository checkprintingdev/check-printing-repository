﻿namespace CheckPrinting
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.gvChequiers = new System.Windows.Forms.DataGridView();
            this.mtSave = new MetroFramework.Controls.MetroTile();
            this.mtDelete = new MetroFramework.Controls.MetroTile();
            this.mtEdit = new MetroFramework.Controls.MetroTile();
            this.mtAdd = new MetroFramework.Controls.MetroTile();
            this.mtRefresh = new MetroFramework.Controls.MetroTile();
            ((System.ComponentModel.ISupportInitialize)(this.gvChequiers)).BeginInit();
            this.SuspendLayout();
            // 
            // gvChequiers
            // 
            this.gvChequiers.AllowUserToAddRows = false;
            this.gvChequiers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gvChequiers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvChequiers.Location = new System.Drawing.Point(32, 172);
            this.gvChequiers.Name = "gvChequiers";
            this.gvChequiers.RowTemplate.Height = 24;
            this.gvChequiers.Size = new System.Drawing.Size(1253, 496);
            this.gvChequiers.TabIndex = 5;
            // 
            // mtSave
            // 
            this.mtSave.BackColor = System.Drawing.Color.DarkOrange;
            //this.mtSave.CustomBackground = true;
            this.mtSave.Location = new System.Drawing.Point(587, 63);
            this.mtSave.Name = "mtSave";
            this.mtSave.Size = new System.Drawing.Size(116, 78);
            this.mtSave.TabIndex = 4;
            this.mtSave.Text = "Save";
            this.mtSave.TileImage = global::CheckPrinting.Properties.Resources.Save;
            this.mtSave.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtSave.UseTileImage = true;
            // 
            // mtDelete
            // 
            this.mtDelete.Location = new System.Drawing.Point(441, 64);
            this.mtDelete.Name = "mtDelete";
            this.mtDelete.Size = new System.Drawing.Size(113, 77);
            this.mtDelete.TabIndex = 3;
            this.mtDelete.Text = "Delete";
            this.mtDelete.TileImage = global::CheckPrinting.Properties.Resources.Delete;
            this.mtDelete.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtDelete.UseTileImage = true;
            // 
            // mtEdit
            // 
            this.mtEdit.Location = new System.Drawing.Point(294, 63);
            this.mtEdit.Name = "mtEdit";
            this.mtEdit.Size = new System.Drawing.Size(112, 78);
            this.mtEdit.TabIndex = 2;
            this.mtEdit.Text = "Edit";
            this.mtEdit.TileImage = global::CheckPrinting.Properties.Resources.Edit;
            this.mtEdit.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtEdit.UseTileImage = true;
            // 
            // mtAdd
            // 
            this.mtAdd.Location = new System.Drawing.Point(161, 64);
            this.mtAdd.Name = "mtAdd";
            this.mtAdd.Size = new System.Drawing.Size(111, 77);
            this.mtAdd.TabIndex = 1;
            this.mtAdd.Text = "Add";
            this.mtAdd.TileImage = global::CheckPrinting.Properties.Resources.Add;
            this.mtAdd.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtAdd.UseTileImage = true;
            this.mtAdd.Click += new System.EventHandler(this.mtAdd_Click);
            // 
            // mtRefresh
            // 
            this.mtRefresh.Location = new System.Drawing.Point(32, 63);
            this.mtRefresh.Name = "mtRefresh";
            this.mtRefresh.Size = new System.Drawing.Size(111, 78);
            this.mtRefresh.TabIndex = 0;
            this.mtRefresh.Text = "Refresh";
            this.mtRefresh.TileImage = global::CheckPrinting.Properties.Resources.Refresh;
            this.mtRefresh.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtRefresh.UseTileImage = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1292, 659);
            this.Controls.Add(this.gvChequiers);
            this.Controls.Add(this.mtSave);
            this.Controls.Add(this.mtDelete);
            this.Controls.Add(this.mtEdit);
            this.Controls.Add(this.mtAdd);
            this.Controls.Add(this.mtRefresh);
            this.Name = "Form1";
            this.Text = "Liste Chéquiers";
            ((System.ComponentModel.ISupportInitialize)(this.gvChequiers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTile mtRefresh;
        private MetroFramework.Controls.MetroTile mtAdd;
        private MetroFramework.Controls.MetroTile mtEdit;
        private MetroFramework.Controls.MetroTile mtDelete;
        private MetroFramework.Controls.MetroTile mtSave;
        private System.Windows.Forms.DataGridView gvChequiers;
    }
}

