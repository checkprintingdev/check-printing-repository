﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using CheckPrinting.Class;
using CheckPrinting_DAL;
using MetroFramework;

namespace CheckPrinting
{
    public partial class frmAddBanque :  MetroFramework.Forms.MetroForm
    {
        string fileName;
        List<Banque> listeBanque;
        public frmAddBanque()
        {
            InitializeComponent();
            this.Size = Screen.FromRectangle(this.Bounds).WorkingArea.Size;
        }

      
        //Convert binary to image
        Image ConvertBinaryToImage(byte[] data)
        {
            using (MemoryStream ms = new MemoryStream(data))
            {
                return Image.FromStream(ms);
            }
        }
        //Convert image to binary
        byte[] ConvertImageToBinary(Image img)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }
        //private void btnParcourir_Click(object sender, EventArgs e)
        //{
        //    //Read image file
        //    using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "JPEG|*.jpg", ValidateNames = true, Multiselect = false })
        //    {
        //        if (ofd.ShowDialog() == DialogResult.OK)
        //        {
        //            fileName = ofd.FileName;
        //            lblFilename.Text = fileName;
        //            pictureBox1.Image = Image.FromFile(fileName);
        //        }
        //    }
        //}
        
        private void loadBanques(string filter)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            List<Banque> listeBanques = new List<Banque>();
            if (filter == "")
            {
                listeBanques = unitOfWork.banqueRepository.Get().ToList();
            }
            else
            {
                listeBanques = unitOfWork.banqueRepository.Get().ToList().Where(x => x.CodeBanque.Contains(filter.Trim()) || x.LibelleBanque.Contains(filter.Trim()) || x.LibelleCourt.ToUpper().Contains(filter.ToUpper().Trim())).ToList();
            }

            listeBanque = listeBanques;
            banqueListView.BeginUpdate();
            banqueListView.Items.Clear();
            banqueListView.View = View.Details;
            banqueListView.CheckBoxes = true;

           
            this.imageListBanques.TransparentColor = Color.Transparent;
            imageCombo1.Items.Add(new ImageComboItem("Select an Item", -1));
            int i = 0;
            //Fill Rows
            foreach (Banque banque in listeBanques)
            {
                ListViewItem lvi;
                lvi = new ListViewItem(new string[] { banque.CodeBanque , banque.LibelleBanque ,banque.LibelleCourt ///,banque.Picture
                });
                banqueListView.Items.Add(lvi);
                banqueListView.Items[0].Checked = true;

                
                this.imageListBanques.Images.Add(i.ToString(),ConvertBinaryToImage(banque.Picture));               
                imageCombo1.Items.Add(new ImageComboItem(banque.LibelleCourt, i));
                i = i + 1;
               
            }
            imageCombo1.ImageList = imageListBanques;
            imageCombo1.SelectedIndex = 0;
            banqueListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            banqueListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            banqueListView.EndUpdate();
            //chequierListView.AllowSorting = true;
        }
        private void metroLink2_Click(object sender, EventArgs e)
        {
            frmParametrage f = new frmParametrage();
            f.Show();
            Hide();
        }

        private void metroLink1_Click(object sender, EventArgs e)
        {
            Close();
            frmParametrage f = new frmParametrage();
            f.Show();
        }

      
       

        private void btnModifier_Click(object sender, EventArgs e)
        {

        }

        private void banqueListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            if (banqueListView.SelectedIndices.Count <= 0)
            {
                return;
            }
            if (banqueListView.FocusedItem != null)
            {
                //Set image to picture box
                pictureBox1.Image = ConvertBinaryToImage(listeBanque[banqueListView.FocusedItem.Index].Picture);
                lblFilename.Text = banqueListView.FocusedItem.SubItems[0].Text;
            }
            ListViewItem item = banqueListView.FocusedItem as ListViewItem;
            string codeBanque = item.Text.ToString();
            Banque banque = unitOfWork.banqueRepository.Get().ToList().Where(x => x.CodeBanque == codeBanque).FirstOrDefault();
            if (banque != null)
            {
                txtCodeBanque.Text = banque.CodeBanque;
                txtLibelleBanque.Text = banque.LibelleBanque;
                txtLibelleCourt.Text = banque.LibelleCourt;

            }
            btnModifier.Visible = true;
            btnSave.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                // open file dialog
                OpenFileDialog open = new OpenFileDialog();
                // image filters
                open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

                if (open.ShowDialog() == DialogResult.OK)
                {
                    // display image in picture box
                    pictureBox1.Image = new Bitmap(open.FileName);
                    // image file path
                    textBox1.Text = open.FileName;
                }
            }
            catch (Exception)
            {
                throw new ApplicationException("Image loading error....");
            }
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            Banque banque = new Banque();
            banque.CodeBanque = txtCodeBanque.Text.ToString().Trim();
            banque.LibelleBanque = txtLibelleBanque.Text.ToString().Trim();
            banque.LibelleCourt = txtLibelleCourt.Text.ToString().Trim();
            banque.Picture = ConvertImageToBinary(pictureBox1.Image);

            string result = unitOfWork.banqueRepository.Insert(banque).ToString();
            if (result == "Unchanged")
            {
                MetroMessageBox.Show(this, "Banque ajoutée avec succès", "Success",
                          MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MetroMessageBox.Show(this, "Une erreur est survenue.", "Error",
                                 MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            loadBanques("");
        }

        private void frmAddBanque_Load_1(object sender, EventArgs e)
        {
            SingleConnection parameterConnection = new SingleConnection();
            parameterConnection.dataSource = ParametreConnection.dataSource;
            parameterConnection.initialCatalogue = ParametreConnection.initialCatalogue;
            parameterConnection.userID = ParametreConnection.userID;
            parameterConnection.password = ParametreConnection.password;
            UnitOfWork unitOfWork = new UnitOfWork(parameterConnection);
            List<Banque> listeBanque = unitOfWork.banqueRepository.Get().ToList();

            loadBanques("");

            toolStripStatusLabel1.Text = DateTime.Now.ToLongDateString();
        }
    }
}
