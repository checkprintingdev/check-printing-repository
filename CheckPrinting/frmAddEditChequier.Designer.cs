﻿namespace CheckPrinting
{
    partial class frmAddEditChequier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddEditChequier));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtRefChequier = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtNumPremChequier = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.chkEtat = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.cmbBanque = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtNumDerChequier = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.txtRIB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.txtNbreCheque = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.txtClient = new MetroFramework.Controls.MetroTextBox();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroComboBox2 = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.txtAdresse = new MetroFramework.Controls.MetroTextBox();
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.metroTabChequier = new MetroFramework.Controls.MetroTabControl();
            this.metroPageChequier = new MetroFramework.Controls.MetroTabPage();
            this.chequierListView = new System.Windows.Forms.ListView();
            this.RefChequier = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TitulaireCompte = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RIB = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NumPreCh = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NumDerCh = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NbreCh = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DateCommandeBanque = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LieuPayement = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AdrPayement = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TelAgence = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Etat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.JourneeBanque = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DateTraitement = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Residence = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodeDevise = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DateLivraison = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DateBLClient = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodeCentreImpr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodeBanque = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodeAgence = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodeProduit = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gvChequiers = new MetroFramework.Controls.MetroGrid();
            this.metroDateTime1 = new MetroFramework.Controls.MetroDateTime();
            this.txtLieuPayement = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.cmbDevise = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.cmbAgence = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.txtTelAgence = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.txtResidence = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.cmbProduit = new MetroFramework.Controls.MetroComboBox();
            this.cmbTypeProduit = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.txtSearch = new MetroFramework.Controls.MetroTextBox();
            this.btnModifier = new MetroFramework.Controls.MetroButton();
            this.imageListBanques = new System.Windows.Forms.ImageList(this.components);
            this.btnGenereLigneCheq = new MetroFramework.Controls.MetroButton();
            this.cmbBanque2 = new System.Windows.Forms.ImageCombo();
            this.statusStrip1.SuspendLayout();
            this.metroTabChequier.SuspendLayout();
            this.metroPageChequier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvChequiers)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(1077, 345);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(124, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Référence Chequier";
            // 
            // txtRefChequier
            // 
            // 
            // 
            // 
            this.txtRefChequier.CustomButton.Image = null;
            this.txtRefChequier.CustomButton.Location = new System.Drawing.Point(548, 2);
            this.txtRefChequier.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtRefChequier.CustomButton.Name = "";
            this.txtRefChequier.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtRefChequier.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtRefChequier.CustomButton.TabIndex = 1;
            this.txtRefChequier.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtRefChequier.CustomButton.UseSelectable = true;
            this.txtRefChequier.CustomButton.Visible = false;
            this.txtRefChequier.Lines = new string[0];
            this.txtRefChequier.Location = new System.Drawing.Point(1227, 342);
            this.txtRefChequier.MaxLength = 32767;
            this.txtRefChequier.Name = "txtRefChequier";
            this.txtRefChequier.PasswordChar = '\0';
            this.txtRefChequier.PromptText = "Référence Chéquier";
            this.txtRefChequier.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtRefChequier.SelectedText = "";
            this.txtRefChequier.SelectionLength = 0;
            this.txtRefChequier.SelectionStart = 0;
            this.txtRefChequier.ShortcutsEnabled = true;
            this.txtRefChequier.Size = new System.Drawing.Size(568, 22);
            this.txtRefChequier.TabIndex = 1;
            this.txtRefChequier.UseSelectable = true;
            this.txtRefChequier.WaterMark = "Référence Chéquier";
            this.txtRefChequier.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtRefChequier.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(1077, 222);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(124, 19);
            this.metroLabel2.TabIndex = 0;
            this.metroLabel2.Text = "N° Premier Cheque";
            // 
            // txtNumPremChequier
            // 
            // 
            // 
            // 
            this.txtNumPremChequier.CustomButton.Image = null;
            this.txtNumPremChequier.CustomButton.Location = new System.Drawing.Point(168, 2);
            this.txtNumPremChequier.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumPremChequier.CustomButton.Name = "";
            this.txtNumPremChequier.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtNumPremChequier.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNumPremChequier.CustomButton.TabIndex = 1;
            this.txtNumPremChequier.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNumPremChequier.CustomButton.UseSelectable = true;
            this.txtNumPremChequier.CustomButton.Visible = false;
            this.txtNumPremChequier.Lines = new string[0];
            this.txtNumPremChequier.Location = new System.Drawing.Point(1227, 222);
            this.txtNumPremChequier.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumPremChequier.MaxLength = 32767;
            this.txtNumPremChequier.Name = "txtNumPremChequier";
            this.txtNumPremChequier.PasswordChar = '\0';
            this.txtNumPremChequier.PromptText = "N° Premier Chèque";
            this.txtNumPremChequier.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNumPremChequier.SelectedText = "";
            this.txtNumPremChequier.SelectionLength = 0;
            this.txtNumPremChequier.SelectionStart = 0;
            this.txtNumPremChequier.ShortcutsEnabled = true;
            this.txtNumPremChequier.Size = new System.Drawing.Size(188, 22);
            this.txtNumPremChequier.TabIndex = 1;
            this.txtNumPremChequier.UseSelectable = true;
            this.txtNumPremChequier.WaterMark = "N° Premier Chèque";
            this.txtNumPremChequier.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNumPremChequier.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNumPremChequier.Leave += new System.EventHandler(this.txtNumPremChequier_Leave);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(1458, 531);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(31, 19);
            this.metroLabel3.TabIndex = 0;
            this.metroLabel3.Text = "Etat";
            // 
            // chkEtat
            // 
            this.chkEtat.AutoSize = true;
            this.chkEtat.Location = new System.Drawing.Point(1607, 531);
            this.chkEtat.Margin = new System.Windows.Forms.Padding(2);
            this.chkEtat.Name = "chkEtat";
            this.chkEtat.Size = new System.Drawing.Size(28, 15);
            this.chkEtat.TabIndex = 2;
            this.chkEtat.Text = "?";
            this.chkEtat.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(1077, 108);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(53, 19);
            this.metroLabel4.TabIndex = 0;
            this.metroLabel4.Text = "Banque";
            // 
            // cmbBanque
            // 
            this.cmbBanque.FormattingEnabled = true;
            this.cmbBanque.ItemHeight = 23;
            this.cmbBanque.Location = new System.Drawing.Point(1221, 785);
            this.cmbBanque.Margin = new System.Windows.Forms.Padding(2);
            this.cmbBanque.Name = "cmbBanque";
            this.cmbBanque.Size = new System.Drawing.Size(188, 29);
            this.cmbBanque.TabIndex = 3;
            this.cmbBanque.UseSelectable = true;
            this.cmbBanque.Visible = false;
            this.cmbBanque.SelectedIndexChanged += new System.EventHandler(this.cmbBanque_SelectedIndexChanged);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(1458, 222);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(120, 19);
            this.metroLabel6.TabIndex = 0;
            this.metroLabel6.Text = "N° Dernier Cheque";
            // 
            // txtNumDerChequier
            // 
            // 
            // 
            // 
            this.txtNumDerChequier.CustomButton.Image = null;
            this.txtNumDerChequier.CustomButton.Location = new System.Drawing.Point(168, 2);
            this.txtNumDerChequier.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumDerChequier.CustomButton.Name = "";
            this.txtNumDerChequier.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtNumDerChequier.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNumDerChequier.CustomButton.TabIndex = 1;
            this.txtNumDerChequier.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNumDerChequier.CustomButton.UseSelectable = true;
            this.txtNumDerChequier.CustomButton.Visible = false;
            this.txtNumDerChequier.Lines = new string[0];
            this.txtNumDerChequier.Location = new System.Drawing.Point(1607, 222);
            this.txtNumDerChequier.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumDerChequier.MaxLength = 32767;
            this.txtNumDerChequier.Name = "txtNumDerChequier";
            this.txtNumDerChequier.PasswordChar = '\0';
            this.txtNumDerChequier.PromptText = "N° Dernier Chèque";
            this.txtNumDerChequier.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNumDerChequier.SelectedText = "";
            this.txtNumDerChequier.SelectionLength = 0;
            this.txtNumDerChequier.SelectionStart = 0;
            this.txtNumDerChequier.ShortcutsEnabled = true;
            this.txtNumDerChequier.Size = new System.Drawing.Size(188, 22);
            this.txtNumDerChequier.TabIndex = 1;
            this.txtNumDerChequier.UseSelectable = true;
            this.txtNumDerChequier.WaterMark = "N° Dernier Chèque";
            this.txtNumDerChequier.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNumDerChequier.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNumDerChequier.Leave += new System.EventHandler(this.txtNumDerChequier_Leave);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(1458, 168);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(28, 19);
            this.metroLabel7.TabIndex = 0;
            this.metroLabel7.Text = "RIB";
            // 
            // txtRIB
            // 
            // 
            // 
            // 
            this.txtRIB.CustomButton.Image = null;
            this.txtRIB.CustomButton.Location = new System.Drawing.Point(168, 2);
            this.txtRIB.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtRIB.CustomButton.Name = "";
            this.txtRIB.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtRIB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtRIB.CustomButton.TabIndex = 1;
            this.txtRIB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtRIB.CustomButton.UseSelectable = true;
            this.txtRIB.CustomButton.Visible = false;
            this.txtRIB.Lines = new string[0];
            this.txtRIB.Location = new System.Drawing.Point(1607, 168);
            this.txtRIB.Margin = new System.Windows.Forms.Padding(2);
            this.txtRIB.MaxLength = 32767;
            this.txtRIB.Name = "txtRIB";
            this.txtRIB.PasswordChar = '\0';
            this.txtRIB.PromptText = "RIB";
            this.txtRIB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtRIB.SelectedText = "";
            this.txtRIB.SelectionLength = 0;
            this.txtRIB.SelectionStart = 0;
            this.txtRIB.ShortcutsEnabled = true;
            this.txtRIB.Size = new System.Drawing.Size(188, 22);
            this.txtRIB.TabIndex = 1;
            this.txtRIB.UseSelectable = true;
            this.txtRIB.WaterMark = "RIB";
            this.txtRIB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtRIB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtRIB.TextChanged += new System.EventHandler(this.txtRIB_TextChanged);
            this.txtRIB.Leave += new System.EventHandler(this.txtRIB_Leave);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(1077, 282);
            this.metroLabel8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(132, 19);
            this.metroLabel8.TabIndex = 0;
            this.metroLabel8.Text = "Nombre de Cheques";
            // 
            // txtNbreCheque
            // 
            // 
            // 
            // 
            this.txtNbreCheque.CustomButton.Image = null;
            this.txtNbreCheque.CustomButton.Location = new System.Drawing.Point(168, 2);
            this.txtNbreCheque.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtNbreCheque.CustomButton.Name = "";
            this.txtNbreCheque.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtNbreCheque.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNbreCheque.CustomButton.TabIndex = 1;
            this.txtNbreCheque.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNbreCheque.CustomButton.UseSelectable = true;
            this.txtNbreCheque.CustomButton.Visible = false;
            this.txtNbreCheque.Lines = new string[0];
            this.txtNbreCheque.Location = new System.Drawing.Point(1227, 279);
            this.txtNbreCheque.Margin = new System.Windows.Forms.Padding(2);
            this.txtNbreCheque.MaxLength = 32767;
            this.txtNbreCheque.Name = "txtNbreCheque";
            this.txtNbreCheque.PasswordChar = '\0';
            this.txtNbreCheque.PromptText = "Nombre de chéquier";
            this.txtNbreCheque.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNbreCheque.SelectedText = "";
            this.txtNbreCheque.SelectionLength = 0;
            this.txtNbreCheque.SelectionStart = 0;
            this.txtNbreCheque.ShortcutsEnabled = true;
            this.txtNbreCheque.Size = new System.Drawing.Size(188, 22);
            this.txtNbreCheque.TabIndex = 1;
            this.txtNbreCheque.UseSelectable = true;
            this.txtNbreCheque.WaterMark = "Nombre de chéquier";
            this.txtNbreCheque.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNbreCheque.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNbreCheque.Leave += new System.EventHandler(this.txtNbreCheque_Leave);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(1458, 279);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(79, 19);
            this.metroLabel9.TabIndex = 0;
            this.metroLabel9.Text = "Date Carnet";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(1077, 168);
            this.metroLabel10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(55, 19);
            this.metroLabel10.TabIndex = 0;
            this.metroLabel10.Text = "Titulaire";
            // 
            // txtClient
            // 
            // 
            // 
            // 
            this.txtClient.CustomButton.Image = null;
            this.txtClient.CustomButton.Location = new System.Drawing.Point(168, 2);
            this.txtClient.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtClient.CustomButton.Name = "";
            this.txtClient.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtClient.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtClient.CustomButton.TabIndex = 1;
            this.txtClient.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtClient.CustomButton.UseSelectable = true;
            this.txtClient.CustomButton.Visible = false;
            this.txtClient.Lines = new string[0];
            this.txtClient.Location = new System.Drawing.Point(1227, 168);
            this.txtClient.Margin = new System.Windows.Forms.Padding(2);
            this.txtClient.MaxLength = 32767;
            this.txtClient.Name = "txtClient";
            this.txtClient.PasswordChar = '\0';
            this.txtClient.PromptText = "Titulaire du compte";
            this.txtClient.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtClient.SelectedText = "";
            this.txtClient.SelectionLength = 0;
            this.txtClient.SelectionStart = 0;
            this.txtClient.ShortcutsEnabled = true;
            this.txtClient.Size = new System.Drawing.Size(188, 22);
            this.txtClient.TabIndex = 4;
            this.txtClient.UseSelectable = true;
            this.txtClient.WaterMark = "Titulaire du compte";
            this.txtClient.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtClient.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(1678, 801);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(117, 36);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(1458, 404);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(33, 19);
            this.metroLabel5.TabIndex = 0;
            this.metroLabel5.Text = "Ville";
            // 
            // metroComboBox2
            // 
            this.metroComboBox2.FormattingEnabled = true;
            this.metroComboBox2.ItemHeight = 23;
            this.metroComboBox2.Location = new System.Drawing.Point(1607, 404);
            this.metroComboBox2.Margin = new System.Windows.Forms.Padding(2);
            this.metroComboBox2.Name = "metroComboBox2";
            this.metroComboBox2.Size = new System.Drawing.Size(188, 29);
            this.metroComboBox2.TabIndex = 3;
            this.metroComboBox2.UseSelectable = true;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(1077, 699);
            this.metroLabel11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(55, 19);
            this.metroLabel11.TabIndex = 0;
            this.metroLabel11.Text = "Adresse";
            // 
            // txtAdresse
            // 
            // 
            // 
            // 
            this.txtAdresse.CustomButton.Image = null;
            this.txtAdresse.CustomButton.Location = new System.Drawing.Point(524, 2);
            this.txtAdresse.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresse.CustomButton.Name = "";
            this.txtAdresse.CustomButton.Size = new System.Drawing.Size(61, 61);
            this.txtAdresse.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAdresse.CustomButton.TabIndex = 1;
            this.txtAdresse.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAdresse.CustomButton.UseSelectable = true;
            this.txtAdresse.CustomButton.Visible = false;
            this.txtAdresse.Lines = new string[0];
            this.txtAdresse.Location = new System.Drawing.Point(1207, 699);
            this.txtAdresse.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresse.MaxLength = 32767;
            this.txtAdresse.Multiline = true;
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.PasswordChar = '\0';
            this.txtAdresse.PromptText = "Adresse";
            this.txtAdresse.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAdresse.SelectedText = "";
            this.txtAdresse.SelectionLength = 0;
            this.txtAdresse.SelectionStart = 0;
            this.txtAdresse.ShortcutsEnabled = true;
            this.txtAdresse.Size = new System.Drawing.Size(588, 66);
            this.txtAdresse.TabIndex = 1;
            this.txtAdresse.UseSelectable = true;
            this.txtAdresse.WaterMark = "Adresse";
            this.txtAdresse.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAdresse.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLink1
            // 
            this.metroLink1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLink1.Image = ((System.Drawing.Image)(resources.GetObject("metroLink1.Image")));
            this.metroLink1.ImageSize = 32;
            this.metroLink1.Location = new System.Drawing.Point(1848, 10);
            this.metroLink1.Margin = new System.Windows.Forms.Padding(2);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.NoFocusImage = ((System.Drawing.Image)(resources.GetObject("metroLink1.NoFocusImage")));
            this.metroLink1.Size = new System.Drawing.Size(24, 26);
            this.metroLink1.TabIndex = 18;
            this.metroLink1.TabStop = false;
            this.metroLink1.UseSelectable = true;
            this.metroLink1.Click += new System.EventHandler(this.metroLink1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            this.statusStrip1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(15, 850);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(1846, 24);
            this.statusStrip1.TabIndex = 19;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.ActiveLinkColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.LinkColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(139, 19);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.VisitedLinkColor = System.Drawing.Color.White;
            // 
            // metroTabChequier
            // 
            this.metroTabChequier.Controls.Add(this.metroPageChequier);
            this.metroTabChequier.Location = new System.Drawing.Point(30, 140);
            this.metroTabChequier.Name = "metroTabChequier";
            this.metroTabChequier.SelectedIndex = 0;
            this.metroTabChequier.Size = new System.Drawing.Size(995, 566);
            this.metroTabChequier.TabIndex = 20;
            this.metroTabChequier.UseSelectable = true;
            // 
            // metroPageChequier
            // 
            this.metroPageChequier.Controls.Add(this.chequierListView);
            this.metroPageChequier.Controls.Add(this.gvChequiers);
            this.metroPageChequier.HorizontalScrollbarBarColor = true;
            this.metroPageChequier.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPageChequier.HorizontalScrollbarSize = 10;
            this.metroPageChequier.Location = new System.Drawing.Point(4, 38);
            this.metroPageChequier.Name = "metroPageChequier";
            this.metroPageChequier.Size = new System.Drawing.Size(987, 524);
            this.metroPageChequier.TabIndex = 0;
            this.metroPageChequier.Text = "Chequier";
            this.metroPageChequier.VerticalScrollbarBarColor = true;
            this.metroPageChequier.VerticalScrollbarHighlightOnWheel = false;
            this.metroPageChequier.VerticalScrollbarSize = 10;
            // 
            // chequierListView
            // 
            this.chequierListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.RefChequier,
            this.TitulaireCompte,
            this.RIB,
            this.NumPreCh,
            this.NumDerCh,
            this.NbreCh,
            this.DateCommandeBanque,
            this.LieuPayement,
            this.AdrPayement,
            this.TelAgence,
            this.Etat,
            this.JourneeBanque,
            this.DateTraitement,
            this.Residence,
            this.CodeDevise,
            this.DateLivraison,
            this.DateBLClient,
            this.CodeCentreImpr,
            this.CodeBanque,
            this.CodeAgence,
            this.CodeProduit});
            this.chequierListView.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chequierListView.FullRowSelect = true;
            this.chequierListView.GridLines = true;
            this.chequierListView.HideSelection = false;
            this.chequierListView.Location = new System.Drawing.Point(3, 16);
            this.chequierListView.Name = "chequierListView";
            this.chequierListView.Size = new System.Drawing.Size(981, 476);
            this.chequierListView.TabIndex = 3;
            this.chequierListView.UseCompatibleStateImageBehavior = false;
            this.chequierListView.View = System.Windows.Forms.View.Details;
            this.chequierListView.SelectedIndexChanged += new System.EventHandler(this.chequierListView_SelectedIndexChanged);
            // 
            // RefChequier
            // 
            this.RefChequier.Text = "REFERENCE CHEQUIER";
            this.RefChequier.Width = 200;
            // 
            // TitulaireCompte
            // 
            this.TitulaireCompte.DisplayIndex = 2;
            this.TitulaireCompte.Text = "Titulaire";
            this.TitulaireCompte.Width = 134;
            // 
            // RIB
            // 
            this.RIB.DisplayIndex = 1;
            this.RIB.Text = "RIB";
            this.RIB.Width = 115;
            // 
            // NumPreCh
            // 
            this.NumPreCh.Text = "PREMIER CHEQUE";
            // 
            // NumDerCh
            // 
            this.NumDerCh.Text = "DERNIER CHEQUE";
            // 
            // NbreCh
            // 
            this.NbreCh.Text = "NBRE CHEQUE";
            // 
            // DateCommandeBanque
            // 
            this.DateCommandeBanque.Text = "DATE COMMANDE";
            // 
            // LieuPayement
            // 
            this.LieuPayement.Text = "LIEU PAYEMENT";
            // 
            // AdrPayement
            // 
            this.AdrPayement.Text = "ADRESSE PAYEMENT";
            // 
            // TelAgence
            // 
            this.TelAgence.Text = "TEL AGENCE";
            // 
            // Etat
            // 
            this.Etat.Text = "ETAT";
            // 
            // JourneeBanque
            // 
            this.JourneeBanque.Text = "JOURNEE BANQUE";
            // 
            // DateTraitement
            // 
            this.DateTraitement.Text = "DATE TRAITEMENT";
            // 
            // Residence
            // 
            this.Residence.Text = "RESIDENCE";
            // 
            // CodeDevise
            // 
            this.CodeDevise.Text = "CODE DEVISE";
            // 
            // DateLivraison
            // 
            this.DateLivraison.Text = "DATE LIVRAISON";
            // 
            // DateBLClient
            // 
            this.DateBLClient.Text = "DATE BL CLIENT";
            // 
            // CodeCentreImpr
            // 
            this.CodeCentreImpr.Text = "CODE CENTRE IMPRESSION";
            // 
            // CodeBanque
            // 
            this.CodeBanque.Text = "CODE BANQUE";
            // 
            // CodeAgence
            // 
            this.CodeAgence.Text = "CODE AGENCE";
            // 
            // CodeProduit
            // 
            this.CodeProduit.Text = "CODE PRODUIT";
            // 
            // gvChequiers
            // 
            this.gvChequiers.AllowUserToAddRows = false;
            this.gvChequiers.AllowUserToDeleteRows = false;
            this.gvChequiers.AllowUserToResizeRows = false;
            this.gvChequiers.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gvChequiers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gvChequiers.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gvChequiers.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvChequiers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gvChequiers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvChequiers.DefaultCellStyle = dataGridViewCellStyle5;
            this.gvChequiers.EnableHeadersVisualStyles = false;
            this.gvChequiers.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gvChequiers.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gvChequiers.Location = new System.Drawing.Point(1, 16);
            this.gvChequiers.Name = "gvChequiers";
            this.gvChequiers.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvChequiers.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.gvChequiers.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gvChequiers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvChequiers.Size = new System.Drawing.Size(985, 512);
            this.gvChequiers.TabIndex = 2;
            this.gvChequiers.Visible = false;
            // 
            // metroDateTime1
            // 
            this.metroDateTime1.Location = new System.Drawing.Point(1607, 282);
            this.metroDateTime1.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTime1.Name = "metroDateTime1";
            this.metroDateTime1.Size = new System.Drawing.Size(188, 29);
            this.metroDateTime1.TabIndex = 35;
            // 
            // txtLieuPayement
            // 
            // 
            // 
            // 
            this.txtLieuPayement.CustomButton.Image = null;
            this.txtLieuPayement.CustomButton.Location = new System.Drawing.Point(168, 2);
            this.txtLieuPayement.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtLieuPayement.CustomButton.Name = "";
            this.txtLieuPayement.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtLieuPayement.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtLieuPayement.CustomButton.TabIndex = 1;
            this.txtLieuPayement.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtLieuPayement.CustomButton.UseSelectable = true;
            this.txtLieuPayement.CustomButton.Visible = false;
            this.txtLieuPayement.Lines = new string[0];
            this.txtLieuPayement.Location = new System.Drawing.Point(1607, 468);
            this.txtLieuPayement.Margin = new System.Windows.Forms.Padding(2);
            this.txtLieuPayement.MaxLength = 32767;
            this.txtLieuPayement.Name = "txtLieuPayement";
            this.txtLieuPayement.PasswordChar = '\0';
            this.txtLieuPayement.PromptText = "Lieu Payement";
            this.txtLieuPayement.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLieuPayement.SelectedText = "";
            this.txtLieuPayement.SelectionLength = 0;
            this.txtLieuPayement.SelectionStart = 0;
            this.txtLieuPayement.ShortcutsEnabled = true;
            this.txtLieuPayement.Size = new System.Drawing.Size(188, 22);
            this.txtLieuPayement.TabIndex = 37;
            this.txtLieuPayement.UseSelectable = true;
            this.txtLieuPayement.WaterMark = "Lieu Payement";
            this.txtLieuPayement.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtLieuPayement.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(1458, 468);
            this.metroLabel12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(93, 19);
            this.metroLabel12.TabIndex = 36;
            this.metroLabel12.Text = "Lieu Payement";
            // 
            // cmbDevise
            // 
            this.cmbDevise.FormattingEnabled = true;
            this.cmbDevise.ItemHeight = 23;
            this.cmbDevise.Location = new System.Drawing.Point(1218, 531);
            this.cmbDevise.Margin = new System.Windows.Forms.Padding(2);
            this.cmbDevise.Name = "cmbDevise";
            this.cmbDevise.Size = new System.Drawing.Size(188, 29);
            this.cmbDevise.TabIndex = 39;
            this.cmbDevise.UseSelectable = true;
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(1077, 531);
            this.metroLabel13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(46, 19);
            this.metroLabel13.TabIndex = 38;
            this.metroLabel13.Text = "Devise";
            // 
            // cmbAgence
            // 
            this.cmbAgence.FormattingEnabled = true;
            this.cmbAgence.ItemHeight = 23;
            this.cmbAgence.Location = new System.Drawing.Point(1607, 108);
            this.cmbAgence.Margin = new System.Windows.Forms.Padding(2);
            this.cmbAgence.Name = "cmbAgence";
            this.cmbAgence.Size = new System.Drawing.Size(188, 29);
            this.cmbAgence.TabIndex = 41;
            this.cmbAgence.UseSelectable = true;
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(1458, 108);
            this.metroLabel14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(53, 19);
            this.metroLabel14.TabIndex = 40;
            this.metroLabel14.Text = "Agence";
            // 
            // txtTelAgence
            // 
            // 
            // 
            // 
            this.txtTelAgence.CustomButton.Image = null;
            this.txtTelAgence.CustomButton.Location = new System.Drawing.Point(168, 2);
            this.txtTelAgence.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtTelAgence.CustomButton.Name = "";
            this.txtTelAgence.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtTelAgence.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtTelAgence.CustomButton.TabIndex = 1;
            this.txtTelAgence.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtTelAgence.CustomButton.UseSelectable = true;
            this.txtTelAgence.CustomButton.Visible = false;
            this.txtTelAgence.Lines = new string[0];
            this.txtTelAgence.Location = new System.Drawing.Point(1218, 404);
            this.txtTelAgence.Margin = new System.Windows.Forms.Padding(2);
            this.txtTelAgence.MaxLength = 32767;
            this.txtTelAgence.Name = "txtTelAgence";
            this.txtTelAgence.PasswordChar = '\0';
            this.txtTelAgence.PromptText = "Tèl Agence";
            this.txtTelAgence.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTelAgence.SelectedText = "";
            this.txtTelAgence.SelectionLength = 0;
            this.txtTelAgence.SelectionStart = 0;
            this.txtTelAgence.ShortcutsEnabled = true;
            this.txtTelAgence.Size = new System.Drawing.Size(188, 22);
            this.txtTelAgence.TabIndex = 43;
            this.txtTelAgence.UseSelectable = true;
            this.txtTelAgence.WaterMark = "Tèl Agence";
            this.txtTelAgence.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtTelAgence.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(1077, 404);
            this.metroLabel15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(116, 19);
            this.metroLabel15.TabIndex = 42;
            this.metroLabel15.Text = "Télephone Agence";
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(1077, 468);
            this.metroLabel16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(67, 19);
            this.metroLabel16.TabIndex = 36;
            this.metroLabel16.Text = "Résidence";
            // 
            // txtResidence
            // 
            // 
            // 
            // 
            this.txtResidence.CustomButton.Image = null;
            this.txtResidence.CustomButton.Location = new System.Drawing.Point(168, 2);
            this.txtResidence.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtResidence.CustomButton.Name = "";
            this.txtResidence.CustomButton.Size = new System.Drawing.Size(17, 17);
            this.txtResidence.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtResidence.CustomButton.TabIndex = 1;
            this.txtResidence.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtResidence.CustomButton.UseSelectable = true;
            this.txtResidence.CustomButton.Visible = false;
            this.txtResidence.Lines = new string[0];
            this.txtResidence.Location = new System.Drawing.Point(1218, 468);
            this.txtResidence.Margin = new System.Windows.Forms.Padding(2);
            this.txtResidence.MaxLength = 32767;
            this.txtResidence.Name = "txtResidence";
            this.txtResidence.PasswordChar = '\0';
            this.txtResidence.PromptText = "Résidence";
            this.txtResidence.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtResidence.SelectedText = "";
            this.txtResidence.SelectionLength = 0;
            this.txtResidence.SelectionStart = 0;
            this.txtResidence.ShortcutsEnabled = true;
            this.txtResidence.Size = new System.Drawing.Size(188, 22);
            this.txtResidence.TabIndex = 37;
            this.txtResidence.UseSelectable = true;
            this.txtResidence.WaterMark = "Résidence";
            this.txtResidence.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtResidence.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(1458, 603);
            this.metroLabel17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(52, 19);
            this.metroLabel17.TabIndex = 38;
            this.metroLabel17.Text = "Produit";
            // 
            // cmbProduit
            // 
            this.cmbProduit.FormattingEnabled = true;
            this.cmbProduit.ItemHeight = 23;
            this.cmbProduit.Location = new System.Drawing.Point(1607, 603);
            this.cmbProduit.Margin = new System.Windows.Forms.Padding(2);
            this.cmbProduit.Name = "cmbProduit";
            this.cmbProduit.Size = new System.Drawing.Size(188, 29);
            this.cmbProduit.TabIndex = 39;
            this.cmbProduit.UseSelectable = true;
            // 
            // cmbTypeProduit
            // 
            this.cmbTypeProduit.FormattingEnabled = true;
            this.cmbTypeProduit.ItemHeight = 23;
            this.cmbTypeProduit.Location = new System.Drawing.Point(1207, 603);
            this.cmbTypeProduit.Margin = new System.Windows.Forms.Padding(2);
            this.cmbTypeProduit.Name = "cmbTypeProduit";
            this.cmbTypeProduit.Size = new System.Drawing.Size(188, 29);
            this.cmbTypeProduit.TabIndex = 45;
            this.cmbTypeProduit.UseSelectable = true;
            this.cmbTypeProduit.SelectedIndexChanged += new System.EventHandler(this.cmbTypeProduit_SelectedIndexChanged);
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(1077, 603);
            this.metroLabel18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(83, 19);
            this.metroLabel18.TabIndex = 44;
            this.metroLabel18.Text = "Type Produit";
            // 
            // txtSearch
            // 
            // 
            // 
            // 
            this.txtSearch.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            this.txtSearch.CustomButton.Location = new System.Drawing.Point(321, 1);
            this.txtSearch.CustomButton.Name = "";
            this.txtSearch.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSearch.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSearch.CustomButton.TabIndex = 1;
            this.txtSearch.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSearch.CustomButton.UseSelectable = true;
            this.txtSearch.DisplayIcon = true;
            this.txtSearch.Icon = ((System.Drawing.Image)(resources.GetObject("txtSearch.Icon")));
            this.txtSearch.Lines = new string[0];
            this.txtSearch.Location = new System.Drawing.Point(682, 115);
            this.txtSearch.MaxLength = 32767;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.PasswordChar = '\0';
            this.txtSearch.PromptText = "Search";
            this.txtSearch.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSearch.SelectedText = "";
            this.txtSearch.SelectionLength = 0;
            this.txtSearch.SelectionStart = 0;
            this.txtSearch.ShortcutsEnabled = true;
            this.txtSearch.ShowButton = true;
            this.txtSearch.Size = new System.Drawing.Size(343, 23);
            this.txtSearch.TabIndex = 46;
            this.txtSearch.UseSelectable = true;
            this.txtSearch.WaterMark = "Search";
            this.txtSearch.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSearch.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // btnModifier
            // 
            this.btnModifier.Location = new System.Drawing.Point(1678, 801);
            this.btnModifier.Margin = new System.Windows.Forms.Padding(2);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(117, 36);
            this.btnModifier.TabIndex = 5;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseSelectable = true;
            this.btnModifier.Visible = false;
            this.btnModifier.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // imageListBanques
            // 
            this.imageListBanques.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageListBanques.ImageSize = new System.Drawing.Size(16, 16);
            this.imageListBanques.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // btnGenereLigneCheq
            // 
            this.btnGenereLigneCheq.Location = new System.Drawing.Point(846, 729);
            this.btnGenereLigneCheq.Margin = new System.Windows.Forms.Padding(2);
            this.btnGenereLigneCheq.Name = "btnGenereLigneCheq";
            this.btnGenereLigneCheq.Size = new System.Drawing.Size(175, 36);
            this.btnGenereLigneCheq.TabIndex = 5;
            this.btnGenereLigneCheq.Text = "GENERER DETAILS CHEQUIERS";
            this.btnGenereLigneCheq.UseSelectable = true;
            this.btnGenereLigneCheq.Click += new System.EventHandler(this.btnGenereLigneCheq_Click);
            // 
            // cmbBanque2
            // 
            this.cmbBanque2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbBanque2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBanque2.FormattingEnabled = true;
            this.cmbBanque2.ItemHeight = 23;
            this.cmbBanque2.Location = new System.Drawing.Point(1227, 108);
            this.cmbBanque2.Name = "cmbBanque2";
            this.cmbBanque2.Size = new System.Drawing.Size(188, 29);
            this.cmbBanque2.TabIndex = 47;
            this.cmbBanque2.SelectedIndexChanged += new System.EventHandler(this.cmbBanque2_SelectedIndexChanged);
            // 
            // frmAddEditChequier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1876, 890);
            this.Controls.Add(this.cmbBanque2);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.cmbTypeProduit);
            this.Controls.Add(this.metroLabel18);
            this.Controls.Add(this.txtTelAgence);
            this.Controls.Add(this.metroLabel15);
            this.Controls.Add(this.cmbAgence);
            this.Controls.Add(this.metroLabel14);
            this.Controls.Add(this.cmbProduit);
            this.Controls.Add(this.metroLabel17);
            this.Controls.Add(this.cmbDevise);
            this.Controls.Add(this.metroLabel13);
            this.Controls.Add(this.txtResidence);
            this.Controls.Add(this.txtLieuPayement);
            this.Controls.Add(this.metroLabel16);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.metroDateTime1);
            this.Controls.Add(this.metroTabChequier);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.metroLink1);
            this.Controls.Add(this.btnGenereLigneCheq);
            this.Controls.Add(this.btnModifier);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtClient);
            this.Controls.Add(this.metroComboBox2);
            this.Controls.Add(this.cmbBanque);
            this.Controls.Add(this.chkEtat);
            this.Controls.Add(this.txtAdresse);
            this.Controls.Add(this.txtRIB);
            this.Controls.Add(this.txtNbreCheque);
            this.Controls.Add(this.txtNumDerChequier);
            this.Controls.Add(this.txtNumPremChequier);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.txtRefChequier);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "frmAddEditChequier";
            this.Padding = new System.Windows.Forms.Padding(15, 60, 15, 16);
            this.Text = "Chequier Infos";
            this.Load += new System.EventHandler(this.frmAddEditChequier_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.metroTabChequier.ResumeLayout(false);
            this.metroPageChequier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvChequiers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtRefChequier;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtNumPremChequier;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroCheckBox chkEtat;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroComboBox cmbBanque;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtNumDerChequier;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox txtRIB;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox txtNbreCheque;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroTextBox txtClient;
        private MetroFramework.Controls.MetroButton btnSave;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroComboBox metroComboBox2;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroTextBox txtAdresse;
        private MetroFramework.Controls.MetroLink metroLink1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private MetroFramework.Controls.MetroTabControl metroTabChequier;
        private MetroFramework.Controls.MetroTabPage metroPageChequier;
        private MetroFramework.Controls.MetroGrid gvChequiers;
        private MetroFramework.Controls.MetroDateTime metroDateTime1;
        private MetroFramework.Controls.MetroTextBox txtLieuPayement;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroComboBox cmbDevise;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroComboBox cmbAgence;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroTextBox txtTelAgence;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroTextBox txtResidence;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private MetroFramework.Controls.MetroComboBox cmbProduit;
        private MetroFramework.Controls.MetroComboBox cmbTypeProduit;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private System.Windows.Forms.ListView chequierListView;
        private System.Windows.Forms.ColumnHeader RefChequier;
        private System.Windows.Forms.ColumnHeader RIB;
        private System.Windows.Forms.ColumnHeader TitulaireCompte;
        private System.Windows.Forms.ColumnHeader NumPreCh;
        private System.Windows.Forms.ColumnHeader NumDerCh;
        private System.Windows.Forms.ColumnHeader NbreCh;
        private System.Windows.Forms.ColumnHeader DateCommandeBanque;
        private System.Windows.Forms.ColumnHeader LieuPayement;
        private System.Windows.Forms.ColumnHeader AdrPayement;
        private System.Windows.Forms.ColumnHeader TelAgence;
        private System.Windows.Forms.ColumnHeader Etat;
        private System.Windows.Forms.ColumnHeader JourneeBanque;
        private System.Windows.Forms.ColumnHeader DateTraitement;
        private System.Windows.Forms.ColumnHeader Residence;
        private System.Windows.Forms.ColumnHeader CodeDevise;
        private System.Windows.Forms.ColumnHeader DateLivraison;
        private System.Windows.Forms.ColumnHeader DateBLClient;
        private System.Windows.Forms.ColumnHeader CodeCentreImpr;
        private System.Windows.Forms.ColumnHeader CodeBanque;
        private System.Windows.Forms.ColumnHeader CodeAgence;
        private System.Windows.Forms.ColumnHeader CodeProduit;
        private MetroFramework.Controls.MetroTextBox txtSearch;
        private MetroFramework.Controls.MetroButton btnModifier;
        private System.Windows.Forms.ImageCombo cmbBanque2;
        private System.Windows.Forms.ImageList imageListBanques;
        private MetroFramework.Controls.MetroButton btnGenereLigneCheq;
    }
}