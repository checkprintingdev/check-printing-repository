﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CheckPrinting.MyFunctions;
using CheckPrinting.Properties;
using MetroFramework.Forms;

namespace CheckPrinting
{
    public partial class frmSetting : MetroForm
    {
        public frmSetting()
        {
            InitializeComponent();
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private void metroLink1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void customButton1_Click(object sender, EventArgs e)
        {
            Settings.Default.Server = tbServer.Text;
            Settings.Default.Port = tbPort.Text;
            Settings.Default.Database = tbDB.Text;
            Settings.Default.Username = tbUname.Text;
            Settings.Default.Password = tbPass.Text;
            Settings.Default.Save();

            if (CheckForInternetConnection())
            {
                if (SQLcmd.TestConnection())
                {
                    MessageBox.Show(this, "Successfully connected to the server!", "Success",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show(this, "Please check your internet connection.", "Error",
                                 MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

           
        }

        private void frmSetting_Load(object sender, EventArgs e)
        {
            tbServer.Text = Settings.Default.Server;
            tbPort.Text = Settings.Default.Port;
            tbDB.Text = Settings.Default.Database;
            tbUname.Text = Settings.Default.Username;
            tbPass.Text = Settings.Default.Password;
        }

     
    }
}
