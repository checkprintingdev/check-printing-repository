﻿namespace CheckPrinting
{
    partial class frmSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetting));
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.tbServer = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.tbPort = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.tbDB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.tbUname = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.tbPass = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.customButton1 = new CustomControls.Controls.CustomButton();
            this.SuspendLayout();
            // 
            // metroLink1
            // 
            this.metroLink1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLink1.Image = ((System.Drawing.Image)(resources.GetObject("metroLink1.Image")));
            this.metroLink1.ImageSize = 32;
            this.metroLink1.Location = new System.Drawing.Point(330, 7);
            this.metroLink1.Margin = new System.Windows.Forms.Padding(2);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.NoFocusImage = ((System.Drawing.Image)(resources.GetObject("metroLink1.NoFocusImage")));
            this.metroLink1.Size = new System.Drawing.Size(24, 26);
            this.metroLink1.TabIndex = 22;
            this.metroLink1.TabStop = false;
            this.metroLink1.UseSelectable = true;
            this.metroLink1.Click += new System.EventHandler(this.metroLink1_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(6, 19);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(183, 25);
            this.metroLabel2.TabIndex = 21;
            this.metroLabel2.Text = "Connection Settings";
            // 
            // tbServer
            // 
            // 
            // 
            // 
            this.tbServer.CustomButton.Image = null;
            this.tbServer.CustomButton.Location = new System.Drawing.Point(208, 2);
            this.tbServer.CustomButton.Name = "";
            this.tbServer.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbServer.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbServer.CustomButton.TabIndex = 1;
            this.tbServer.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbServer.CustomButton.UseSelectable = true;
            this.tbServer.CustomButton.Visible = false;
            this.tbServer.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.tbServer.Lines = new string[0];
            this.tbServer.Location = new System.Drawing.Point(102, 65);
            this.tbServer.MaxLength = 32767;
            this.tbServer.Name = "tbServer";
            this.tbServer.PasswordChar = '\0';
            this.tbServer.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbServer.SelectedText = "";
            this.tbServer.SelectionLength = 0;
            this.tbServer.SelectionStart = 0;
            this.tbServer.ShortcutsEnabled = true;
            this.tbServer.Size = new System.Drawing.Size(232, 26);
            this.tbServer.TabIndex = 24;
            this.tbServer.UseSelectable = true;
            this.tbServer.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbServer.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(22, 72);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(54, 19);
            this.metroLabel1.TabIndex = 23;
            this.metroLabel1.Text = "Server :";
            // 
            // tbPort
            // 
            // 
            // 
            // 
            this.tbPort.CustomButton.Image = null;
            this.tbPort.CustomButton.Location = new System.Drawing.Point(208, 2);
            this.tbPort.CustomButton.Name = "";
            this.tbPort.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbPort.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbPort.CustomButton.TabIndex = 1;
            this.tbPort.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbPort.CustomButton.UseSelectable = true;
            this.tbPort.CustomButton.Visible = false;
            this.tbPort.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.tbPort.Lines = new string[0];
            this.tbPort.Location = new System.Drawing.Point(102, 97);
            this.tbPort.MaxLength = 32767;
            this.tbPort.Name = "tbPort";
            this.tbPort.PasswordChar = '\0';
            this.tbPort.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbPort.SelectedText = "";
            this.tbPort.SelectionLength = 0;
            this.tbPort.SelectionStart = 0;
            this.tbPort.ShortcutsEnabled = true;
            this.tbPort.Size = new System.Drawing.Size(232, 26);
            this.tbPort.TabIndex = 26;
            this.tbPort.UseSelectable = true;
            this.tbPort.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbPort.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.Location = new System.Drawing.Point(22, 104);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(42, 19);
            this.metroLabel3.TabIndex = 25;
            this.metroLabel3.Text = "Port :";
            // 
            // tbDB
            // 
            // 
            // 
            // 
            this.tbDB.CustomButton.Image = null;
            this.tbDB.CustomButton.Location = new System.Drawing.Point(208, 2);
            this.tbDB.CustomButton.Name = "";
            this.tbDB.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbDB.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbDB.CustomButton.TabIndex = 1;
            this.tbDB.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbDB.CustomButton.UseSelectable = true;
            this.tbDB.CustomButton.Visible = false;
            this.tbDB.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.tbDB.Lines = new string[0];
            this.tbDB.Location = new System.Drawing.Point(102, 129);
            this.tbDB.MaxLength = 32767;
            this.tbDB.Name = "tbDB";
            this.tbDB.PasswordChar = '\0';
            this.tbDB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbDB.SelectedText = "";
            this.tbDB.SelectionLength = 0;
            this.tbDB.SelectionStart = 0;
            this.tbDB.ShortcutsEnabled = true;
            this.tbDB.Size = new System.Drawing.Size(232, 26);
            this.tbDB.TabIndex = 28;
            this.tbDB.UseSelectable = true;
            this.tbDB.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbDB.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.Location = new System.Drawing.Point(22, 136);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(73, 19);
            this.metroLabel4.TabIndex = 27;
            this.metroLabel4.Text = "Database :";
            // 
            // tbUname
            // 
            // 
            // 
            // 
            this.tbUname.CustomButton.Image = null;
            this.tbUname.CustomButton.Location = new System.Drawing.Point(208, 2);
            this.tbUname.CustomButton.Name = "";
            this.tbUname.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbUname.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbUname.CustomButton.TabIndex = 1;
            this.tbUname.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbUname.CustomButton.UseSelectable = true;
            this.tbUname.CustomButton.Visible = false;
            this.tbUname.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.tbUname.Lines = new string[0];
            this.tbUname.Location = new System.Drawing.Point(102, 161);
            this.tbUname.MaxLength = 32767;
            this.tbUname.Name = "tbUname";
            this.tbUname.PasswordChar = '\0';
            this.tbUname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbUname.SelectedText = "";
            this.tbUname.SelectionLength = 0;
            this.tbUname.SelectionStart = 0;
            this.tbUname.ShortcutsEnabled = true;
            this.tbUname.Size = new System.Drawing.Size(232, 26);
            this.tbUname.TabIndex = 30;
            this.tbUname.UseSelectable = true;
            this.tbUname.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbUname.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel5.Location = new System.Drawing.Point(22, 168);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(78, 19);
            this.metroLabel5.TabIndex = 29;
            this.metroLabel5.Text = "Username :";
            // 
            // tbPass
            // 
            // 
            // 
            // 
            this.tbPass.CustomButton.Image = null;
            this.tbPass.CustomButton.Location = new System.Drawing.Point(208, 2);
            this.tbPass.CustomButton.Name = "";
            this.tbPass.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbPass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbPass.CustomButton.TabIndex = 1;
            this.tbPass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbPass.CustomButton.UseSelectable = true;
            this.tbPass.CustomButton.Visible = false;
            this.tbPass.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.tbPass.Lines = new string[0];
            this.tbPass.Location = new System.Drawing.Point(102, 193);
            this.tbPass.MaxLength = 32767;
            this.tbPass.Name = "tbPass";
            this.tbPass.PasswordChar = '\0';
            this.tbPass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbPass.SelectedText = "";
            this.tbPass.SelectionLength = 0;
            this.tbPass.SelectionStart = 0;
            this.tbPass.ShortcutsEnabled = true;
            this.tbPass.Size = new System.Drawing.Size(232, 26);
            this.tbPass.TabIndex = 32;
            this.tbPass.UseSelectable = true;
            this.tbPass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbPass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel6.Location = new System.Drawing.Point(22, 200);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(74, 19);
            this.metroLabel6.TabIndex = 31;
            this.metroLabel6.Text = "Password :";
            // 
            // customButton1
            // 
            this.customButton1.AnimationSizeDecrement = 15F;
            this.customButton1.AnimationSizeIncrement = 30F;
            this.customButton1.BackColor = System.Drawing.Color.Transparent;
            this.customButton1.BorderRadius = 4;
            this.customButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.customButton1.FontColor = System.Drawing.Color.White;
            this.customButton1.Location = new System.Drawing.Point(22, 225);
            this.customButton1.Name = "customButton1";
            this.customButton1.Size = new System.Drawing.Size(312, 36);
            this.customButton1.TabIndex = 33;
            this.customButton1.Text = "Test Connection";
            this.customButton1.Click += new System.EventHandler(this.customButton1_Click);
            // 
            // frmSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 281);
            this.ControlBox = false;
            this.Controls.Add(this.customButton1);
            this.Controls.Add(this.tbPass);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.tbUname);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.tbDB);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.tbPort);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.tbServer);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroLink1);
            this.Controls.Add(this.metroLabel2);
            this.DisplayHeader = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSetting";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Load += new System.EventHandler(this.frmSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLink metroLink1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        public MetroFramework.Controls.MetroTextBox tbServer;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        public MetroFramework.Controls.MetroTextBox tbPort;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        public MetroFramework.Controls.MetroTextBox tbDB;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        public MetroFramework.Controls.MetroTextBox tbUname;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        public MetroFramework.Controls.MetroTextBox tbPass;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        public CustomControls.Controls.CustomButton customButton1;
    }
}