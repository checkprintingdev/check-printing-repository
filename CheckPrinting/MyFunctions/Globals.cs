﻿using System;

namespace CheckPrinting.MyFunctions
{
    public static class Globals
    {
        //public static double fare, nos, tam, nocs;
        //public static List<string> seats = new List<string>();
        //public static List<string> cabs = new List<string>();
        //public static string shipname, destination;
        //public static DateTime dot;
        //public static DateTime tdate;
        //public static DateTime ttime;
        public static string voyageCode, route, ship, fare, schedid;
        public static DateTime tdate, ttime;

        public static Random rand = new Random();

        public const string Alphabet =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


        public static string GenerateString(int size)
        {
            char[] chars = new char[size];
            for (int i = 0; i < size; i++)
            {
                chars[i] = Alphabet[rand.Next(Alphabet.Length)];
            }
            return new string(chars);
        }
    }
}
