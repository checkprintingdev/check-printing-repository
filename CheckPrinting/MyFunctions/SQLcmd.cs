﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Windows.Forms;
using CheckPrinting.Properties;

namespace CheckPrinting.MyFunctions
{
    public static class SQLcmd
    {

        public static OleDbConnection _conn = new OleDbConnection();
        public static OleDbCommand _cmd ;
        public static OleDbDataReader _reader;
        public static OleDbDataAdapter _adapter = new OleDbDataAdapter();

        public static string ConnString
        {
            get
            {
                return "Server=" + Settings.Default.Server + ";Userid=" + Settings.Default.Username +
                    ";Password=" + Settings.Default.Password + ";Database=" + Settings.Default.Database +
                    ";Port=" + Settings.Default.Port + ";";
            }
        }

        public static bool TestConnection()
        {
            bool b = true;
            using (OleDbConnection con = new OleDbConnection(ConnString))
            {
                try
                {
                    con.Open();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString(), "Error",
                          MessageBoxButtons.OK, MessageBoxIcon.Error);
                    b = false;
                }
            }
            return b;
        }

        public static DataTable SQLDataTable(string q)
        {
            OleDbConnection Con = null;
            OleDbCommand Command = null;
            OleDbDataAdapter Adapter = null;
            DataTable DTable = new DataTable();

            try
            {
                Con = new OleDbConnection(ConnString);
                if (Con.State == ConnectionState.Closed)
                    Con.Open();
                try
                {
                    Command = new OleDbCommand(q, Con);
                    Adapter = new OleDbDataAdapter(Command);
                    Adapter.Fill(DTable);
                    Con.Close();
                    Con = null;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Data Read Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    DTable = null;
                    Adapter = null;
                    Con.Close();
                    Con = null;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Data Read Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                DTable = null;
                Adapter = null;
                Con.Close();
                Con = null;
            }

            return DTable;
        }

        public static bool SQLExecute(string q)
        {
            OleDbConnection Con = null;
            OleDbCommand Command = null;
            OleDbTransaction trans = null;
            bool Mode = false;

            try
            {
                Con = new OleDbConnection(ConnString);
                if (Con.State == ConnectionState.Closed)
                    Con.Open();
                Command = new OleDbCommand(q, Con);
                Command.ExecuteNonQuery();
                trans = Con.BeginTransaction(IsolationLevel.ReadCommitted);
                Command.Transaction = trans;
                try
                {
                    trans.Commit();
                    Mode = true;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Data Read Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    trans.Rollback();
                    Mode = false;
                }
                Con.Close();
                Con = null;
                Command = null;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Data Read Error",
                       MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Con.Close();
                Con = null;
                Command = null;
            }
            return Mode;
        }
    }
}
