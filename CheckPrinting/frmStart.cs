﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;

namespace CheckPrinting
{
    public partial class frmStart : MetroForm
    {

        public frmStart()
        {
            InitializeComponent();
            this.Size = Screen.FromRectangle(this.Bounds).WorkingArea.Size;
        }

        private void AddForm(UserControl c)
        {
            c.Height = this.Height - 100;
            c.Width = this.Width;
            c.Top = 75;
            this.Controls.Add(c);
            c.BringToFront();
        }

        private void metroLink1_Click(object sender, EventArgs e)
        {
            var ans = MetroMessageBox.Show(this, "Voulez vous quitter l'application?", "Warning!",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (ans == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void frmStart_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = DateTime.Now.ToLongDateString();
        }

        private void metroLink2_Click(object sender, EventArgs e)
        {
            frmSetting f = new frmSetting();
            f.ShowDialog();
            f.Dispose();
            f = null;
        }

        private void modernButton1_Click(object sender, EventArgs e)
        {
            //AddForm(new ucSeat());
            frmGestionChequiers f = new frmGestionChequiers();
            f.Show();
            this.Hide();
        }

        private void customButton1_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void modernButton2_Click(object sender, EventArgs e)
        {
            //AddForm(new ucCabin());
        }

        private void modernButton3_Click(object sender, EventArgs e)
        {
            frmParametrage f = new frmParametrage();
            f.Show();
            this.Hide();
        }
    }
}
