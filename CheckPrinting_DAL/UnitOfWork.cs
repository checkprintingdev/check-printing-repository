﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckPrinting_DAL
{
  public  class UnitOfWork
    {
        public EditionChequeEntities context;
        public GenericRepository<Utilisateur> utilisateurRepository;
        public GenericRepository<Chequier> chequierRepository ;
        public GenericRepository<Banque> banqueRepository;
        public GenericRepository<Agence> agenceRepository ;
        public GenericRepository<Produit> produitRepository ;
        public GenericRepository<TypeProduit> typeProduitRepository ;
        public GenericRepository<Devise> deviseRepository;
        public GenericRepository<DetailsChequier> detailsChequierRepository;
        public UnitOfWork(SingleConnection parameterConnection)
        {
            context = new EditionChequeEntities(parameterConnection.Connect());
            utilisateurRepository = new GenericRepository<Utilisateur>(parameterConnection);
            chequierRepository = new GenericRepository<Chequier>(parameterConnection);
            banqueRepository = new GenericRepository<Banque>(parameterConnection);
            agenceRepository = new GenericRepository<Agence>(parameterConnection);
            produitRepository = new GenericRepository<Produit>(parameterConnection);
            typeProduitRepository = new GenericRepository<TypeProduit>(parameterConnection);
            deviseRepository = new GenericRepository<Devise>(parameterConnection);
            detailsChequierRepository = new GenericRepository<DetailsChequier>(parameterConnection);

        }
        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
