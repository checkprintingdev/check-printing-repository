﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckPrinting_DAL
{
    public class SingleConnection
    {
        public  string dataSource;
        public  string initialCatalogue;
        public  string userID;
        public  string password;
        public SingleConnection()
        {

        }
        public  string Connect()
        {
            //Build an SQL connection string
            SqlConnectionStringBuilder sqlString = new SqlConnectionStringBuilder()
            {
                DataSource = dataSource, // Server name
                InitialCatalog = initialCatalogue,  //Database
                UserID = userID,         //Username
                Password = password,  //Password
            };
            //Build an Entity Framework connection string
            EntityConnectionStringBuilder entityString = new EntityConnectionStringBuilder()
            {
                Provider = "System.Data.SqlClient",
                Metadata = "res://*/EditionChequier.csdl|res://*/EditionChequier.ssdl|res://*/EditionChequier.msl",
                ProviderConnectionString = sqlString.ConnectionString     //  @"data source=SIPL35\SQL2016;initial catalog=Join8ShopDB2;user id=Sa;password=Sa123!@#;"// sqlString.ToString()
            };
            return entityString.ConnectionString;
        }
    }
}
